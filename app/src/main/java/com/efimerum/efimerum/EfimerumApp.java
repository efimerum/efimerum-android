package com.efimerum.efimerum;

import android.app.ActivityManager;
import android.app.Application;

import com.efimerum.efimerum.manager.communication.ServerEventsManager;
import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.model.User;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

import static android.content.pm.ApplicationInfo.FLAG_LARGE_HEAP;
import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.HONEYCOMB;

public class EfimerumApp extends Application {

    private User currentUser;
    private ServerEventsManager eventsManager;

    @Override
    public void onCreate() {
        super.onCreate();

        Picasso picasso = new Picasso.Builder(this)
                .memoryCache(new LruCache(calculateMemoryCacheSize()))
                .build();
        Picasso.setSingletonInstance(picasso);

        Picasso.with(getApplicationContext()).setLoggingEnabled(true);

        DatabaseManager.initializeDatabase(this);

        eventsManager = new ServerEventsManager();
    }

    private int calculateMemoryCacheSize() {
        ActivityManager am = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        boolean largeHeap = (getApplicationInfo().flags & FLAG_LARGE_HEAP) != 0;
        int memoryClass = am.getMemoryClass();
        if (largeHeap && SDK_INT >= HONEYCOMB) {
            memoryClass = am.getLargeMemoryClass();
        }

        // Target ~50% of the available heap.
        return 1024 * 1024 * memoryClass / 2;
    }

    public ServerEventsManager getEventsManager() {
        return eventsManager;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
