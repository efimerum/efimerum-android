package com.efimerum.efimerum.interactor;

import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.util.MainThread;

import java.util.List;

public class GetTagsInteractor {
    public interface GetTagsInteractorResponse {
        void response(List<Tag> tags);
    }

    public void execute(final GetTagsInteractorResponse response) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<Tag> tags = DatabaseManager.getInstance().getTags();

                MainThread.run(new Runnable() {
                    @Override
                    public void run() {
                        response.response(tags);
                    }
                });
            }
        }).run();
    }
}
