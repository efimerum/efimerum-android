package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;

import java.util.List;

public class StorageInitialDataInteractor {
    public interface StorageInitialDataInteractorResponse {
        void response(boolean succeded);
    }

    public void execute(final Photos photos, final List<Tag> tags, final StorageInitialDataInteractorResponse response) {
        final DatabaseManager databaseManager = DatabaseManager.getInstance();
        databaseManager.removeAllData(new DatabaseManager.UpdateOperationResponse() {
            @Override
            public void result(boolean succeded) {
                if (!succeded) {
                    response.response(false);
                }

                databaseManager.insertInitialData(photos, tags, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        response.response(succeded);
                    }
                });
            }
        });
    }
}
