package com.efimerum.efimerum.interactor;

import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.model.Photo;

public class GetPhotoInteractor {

    public void execute(final String photoUUID, final GetPhotoInteractorResponse response) {
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        final Photo photo = databaseManager.getPhoto(photoUUID);
        response.getPhoto(photo);
    }

    public interface GetPhotoInteractorResponse {
        void getPhoto(Photo photo);
    }
}