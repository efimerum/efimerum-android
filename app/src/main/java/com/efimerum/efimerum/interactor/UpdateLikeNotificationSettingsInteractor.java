package com.efimerum.efimerum.interactor;

import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;

public class UpdateLikeNotificationSettingsInteractor {

    public interface UpdateLikeNotificationSettingsInteractorResponse {
        void response(User user);
    }

    public void execute(final User user, boolean notificationEnabled, final UpdateLikeNotificationSettingsInteractorResponse response) {
        user.setLikeNotificationEnabled(notificationEnabled);
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        FBUser firebaseUser = new FirebaseUserUserMapper().unMap(user);
        firebaseHelper.updateUserInfo(firebaseUser, new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser fbUser) {
                User user = new FirebaseUserUserMapper().map(fbUser);
                response.response(user);
            }
        });
    }
}