package com.efimerum.efimerum.interactor;


import android.app.Activity;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.model.User;

public class SetPhotoViewedInteractor {
    public interface SetPhotoViewedInteractorResponse {
        void response(boolean succeded);
    }

    public void execute(Activity activity, String photoUUID, boolean liked, SetPhotoViewedInteractorResponse response) {
        EfimerumApp app = (EfimerumApp) activity.getApplication();
        User user = app.getCurrentUser();
        String userUUID = user != null && !user.isAnonymous() ? user.getUid() : null;

        execute(userUUID, photoUUID, liked, response);
    }

    public void execute(String userId, String photoUUID, boolean liked, final SetPhotoViewedInteractorResponse response) {
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        databaseManager.addPhotoViewed(photoUUID, userId, liked, new DatabaseManager.UpdateOperationResponse() {
            @Override
            public void result(boolean succeded) {
                response.response(succeded);
            }
        });
    }
}
