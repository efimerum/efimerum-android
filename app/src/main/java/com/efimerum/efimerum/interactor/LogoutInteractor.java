package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;

public class LogoutInteractor {

    public interface LogoutInteractorResponse {
        void response(User user);
    }

    public void execute(final User user, final LogoutInteractorResponse response) {
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        FBUser firebaseUser = new FirebaseUserUserMapper().unMap(user);
        firebaseHelper.logout(firebaseUser, new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser fbUser) {
                FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
                firebaseHelper.getCurrentUser(new FirebaseHelper.FirebaseUserListener() {
                    @Override
                    public void getUser(boolean succeded, FBUser firebaseUser) {
                        response.response(new FirebaseUserUserMapper().map(firebaseUser));
                    }
                });
            }
        });
    }
}
