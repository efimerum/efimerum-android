package com.efimerum.efimerum.interactor;


import android.app.Activity;

import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.manager.net.NetworkManager;
import com.efimerum.efimerum.manager.net.PostRequestResult;

public class ReportPhotoInteractor {
    public interface ReportPhotoInteractorResponse {
        void response(boolean succeded);
    }

    public void execute(final Activity activity, final String photoUUID, final String reportCode, final ReportPhotoInteractorResponse response) {
        FirebaseHelper helper = FirebaseHelper.getInstance();
        helper.getToken(new FirebaseHelper.FirebaseTokenListener() {
            @Override
            public void getToken(boolean succeded, final String token) {
                if (!succeded) {
                    response.response(false);
                    return;
                }

                NetworkManager networkManager = new NetworkManager(activity);
                networkManager.reportPhoto(photoUUID, reportCode, token, new NetworkManager.OnPostRequestCompleted() {
                    @Override
                    public void postRequestCompleted(PostRequestResult result) {
                        response.response(result != null && result.isSucceeded());
                    }
                });
            }
        });
    }
}
