package com.efimerum.efimerum.interactor;


import android.app.Activity;
import android.net.Uri;

import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.manager.images.ImagesManager;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;
import com.efimerum.efimerum.util.MainThread;

import java.io.File;

public class UploadAvatarInteractor {
    public void execute(final Activity activity, final User user, final String imagePath, final UploadAvatarInteractorResponse response) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String scaledImagepath = ImagesManager.scaleAndRotateImage(activity, imagePath);
                if (scaledImagepath == null) {
                    giveRespone(response, null);
                    return;
                }

                Uri profileImageUrl = Uri.fromFile(new File(scaledImagepath));

                FirebaseHelper helper = FirebaseHelper.getInstance();
                helper.updateAvatar(new FirebaseUserUserMapper().unMap(user), profileImageUrl.toString(), new FirebaseHelper.FirebaseUserListener() {
                    @Override
                    public void getUser(boolean succeded, FBUser fbUser) {
                        giveRespone(response, fbUser);
                    }
                });
            }
        }).start();
    }

    private void giveRespone(final UploadAvatarInteractorResponse response, final FBUser fbUser) {
        MainThread.run(new Runnable() {
            @Override
            public void run() {
                User x = fbUser != null ? new FirebaseUserUserMapper().map(fbUser) : null;
                response.response(x);
            }
        });
    }

    public interface UploadAvatarInteractorResponse {
        void response(User user);
    }
}
