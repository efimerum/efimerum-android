package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.communication.ServerEventsManager;
import com.efimerum.efimerum.manager.firebase.FBPhoto;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.mappers.PhotoFBPhotoMapper;
import com.efimerum.efimerum.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class DownloadInitialDataInteractor {

    public interface DownloadInitialDataInteractorResponse {
        void response(Photos photos, List<Tag> tags);
    }

    public void execute(ServerEventsManager eventsManager, final DownloadInitialDataInteractorResponse response) {
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        firebaseHelper.initializeFirebaseDatabase(Constants.appLanguage, eventsManager.getPhotosEventListener(), eventsManager.getTagsEventListener(), new FirebaseHelper.FirebaseDatabaseInitializationListener() {
            @Override
            public void initializationResult(boolean succeded, List<FBPhoto> fbPhotos, List<String> fbTags) {
                if (!succeded || fbPhotos == null || fbTags == null) {
                    response.response(null, null);
                    return;
                }

                List<Photo> list = new ArrayList<>();

                for (FBPhoto fbPhoto: fbPhotos) {
                    Photo p = new PhotoFBPhotoMapper().map(fbPhoto);
                    list.add(p);
                }

                Photos photos = Photos.build(list);

                List<Tag> tags = new ArrayList<>();

                for (String tagName: fbTags) {
                    tags.add(new Tag(tagName));
                }

                response.response(photos, tags);
            }
        });
    }
}
