package com.efimerum.efimerum.interactor;

import android.app.Activity;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.manager.firebase.FBPhoto;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.Filter;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.PhotoFBPhotoMapper;
import com.efimerum.efimerum.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class GetPhotosInteractor {

    public static final int ONLY_MY_POSTED_PHOTOS = 3333;
    public static final int ONLY_MY_LIKED_PHOTOS = 3334;

    public void execute(final Activity activity, final Tag tag, final Filter filter, Double latitude, Double longitude, final GetPhotosInteractorResponse response) {
        final DatabaseManager databaseManager = DatabaseManager.getInstance();

        EfimerumApp app = (EfimerumApp) activity.getApplication();
        User user = app.getCurrentUser();

        final String userUUID = user != null && !user.isAnonymous() ? user.getUid() : null;

        if (filter != null) {
            switch (filter.getId()) {
                case R.id.main_activity_more_life_filter: {
                    response.getPhotos(databaseManager.getAllPhotos(userUUID, tag, "expiration_date", true));
                }
                break;
                case R.id.main_activity_about_to_die_filter: {
                    response.getPhotos(databaseManager.getAllPhotos(userUUID, tag, "expiration_date", false));
                }
                break;
                case R.id.main_activity_most_liked_filter: {
                    response.getPhotos(databaseManager.getAllPhotos(userUUID, tag, "likes_count", true));
                }
                break;
                case R.id.main_activity_nearest_filter: {
                    if (latitude == null || longitude == null) {
                        response.getPhotos(Photos.build(new ArrayList<Photo>()));
                        return;
                    }

                    int radius = Integer.parseInt(activity.getString(R.string.config_near_potos_radius));
                    String tagName = tag != null ? tag.getText() : null;

                    FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
                    firebaseHelper.getPhotosByLocation(tagName, Constants.appLanguage, latitude, longitude, radius, new FirebaseHelper.FirebaseNearPhotosListener() {
                        @Override
                        public void nearPhotos(boolean succeded, List<String> photos) {
                            if (!succeded) {
                                response.getPhotos(Photos.build(new ArrayList<Photo>()));
                                return;
                            }

                            response.getPhotos(databaseManager.getAllPhotos(userUUID, tag, photos));
                        }
                    });
                }
                break;
                case ONLY_MY_POSTED_PHOTOS: {
                    response.getPhotos(databaseManager.getAllPhotos(userUUID, tag, null, false, true));
                }
                break;
                case ONLY_MY_LIKED_PHOTOS: {
                    FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
                    firebaseHelper.getLikedPhotos(userUUID, new FirebaseHelper.FirebasePhotosListener() {
                        @Override
                        public void getPhotos(boolean succeded, List<FBPhoto> photos) {
                            if (!succeded) {
                                response.getPhotos(Photos.build(new ArrayList<Photo>()));
                            }

                            List<Photo> list = new ArrayList<>();

                            for (FBPhoto fbPhoto : photos) {
                                Photo p = new PhotoFBPhotoMapper().map(fbPhoto);
                                list.add(p);
                            }

                            response.getPhotos(Photos.build(list));
                        }
                    });
                }
                break;
            }
        } else {
            response.getPhotos(databaseManager.getAllPhotos(userUUID, tag));
        }
    }

    public interface GetPhotosInteractorResponse {
        void getPhotos(Photos photos);
    }
}
