package com.efimerum.efimerum.interactor;


import android.app.Activity;

import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.manager.images.ImagesManager;
import com.efimerum.efimerum.manager.net.NetworkManager;
import com.efimerum.efimerum.manager.net.PostRequestResult;

public class UploadPhotoInteractor {
    public void execute(final Activity activity, final String imagePath, final Double latitude, final Double longitude, final UploadPhotoInteractorResponse response) {
        FirebaseHelper helper = FirebaseHelper.getInstance();
        helper.getToken(new FirebaseHelper.FirebaseTokenListener() {
            @Override
            public void getToken(boolean succeded, final String token) {
                if (!succeded) {
                    response.response(false);
                    return;
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final String scaledImagepath = ImagesManager.scaleAndRotateImage(activity, imagePath);
                        if (scaledImagepath == null) {
                            response.response(false);
                            return;
                        }

                        final NetworkManager networkManager = new NetworkManager(activity);
                        networkManager.uploadPhotoToServer(scaledImagepath, latitude, longitude, token, new NetworkManager.OnPostRequestCompleted() {
                            @Override
                            public void postRequestCompleted(PostRequestResult result) {
                                response.response(result != null && result.isSucceeded());
                            }
                        });

                    }
                }).start();
            }
        });
    }

    public interface UploadPhotoInteractorResponse {
        void response(boolean succeded);
    }
}
