package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;

public class RegisterUserInteractor {

    public interface RegisterUserInteractorResponse {
        void response(User user);
    }

    public void execute(final User user, String password, final RegisterUserInteractorResponse response) {
        final FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        firebaseHelper.registerUser(new FirebaseUserUserMapper().unMap(user), password, new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser firebaseUser) {
                response.response(new FirebaseUserUserMapper().map(firebaseUser));
            }
        });
    }
}
