package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;

public class LoadUserInteractor {

    public interface LoadUserInteractorResponse {
        void response(User user);
    }

    public void execute(final LoadUserInteractorResponse response) {
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        firebaseHelper.getCurrentUser(new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser firebaseUser) {
                response.response(new FirebaseUserUserMapper().map(firebaseUser));
            }
        });
    }
}
