package com.efimerum.efimerum.interactor;


import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;

public class GetPhotoOwnerInteractor {
    public interface GetPhotoOwnerInteractorResponse {
        void response(String ownerName);
    }

    public void execute(String ownerUUID, final GetPhotoOwnerInteractorResponse response) {
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        firebaseHelper.getUserInformation(ownerUUID, new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser fbUser) {
                response.response(succeded && fbUser != null ? fbUser.name : null);
            }
        });
    }
}
