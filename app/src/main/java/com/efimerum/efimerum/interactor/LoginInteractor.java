package com.efimerum.efimerum.interactor;

import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.model.mappers.FirebaseUserUserMapper;

public class LoginInteractor {

    public interface LoginInteractorResponse {
        void response(User user);
    }

    public void execute(String email, String password, final LoginInteractorResponse response) {
        FirebaseHelper firebaseHelper = FirebaseHelper.getInstance();
        firebaseHelper.login(email, password, new FirebaseHelper.FirebaseUserListener() {
            @Override
            public void getUser(boolean succeded, FBUser firebaseUser) {
                User user = new FirebaseUserUserMapper().map(firebaseUser);
                response.response(user);
            }
        });
    }
}
