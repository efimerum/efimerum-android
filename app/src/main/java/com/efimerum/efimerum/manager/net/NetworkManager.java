package com.efimerum.efimerum.manager.net;


import android.content.Context;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.util.LogWriter;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {

    private WeakReference<Context> context;
    private APIClient apiClient;
    public NetworkManager(Context context) {
        this.context = new WeakReference<Context>(context);
        String serverURL = context.getString(R.string.config_server_url);
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(serverURL)
                .build();
        apiClient = retrofit.create(APIClient.class);
    }

    public void uploadPhotoToServer(String filePath, Double latitude, Double longitude, String token, final OnPostRequestCompleted listener) {
        try {
            File file = new File(filePath);

            RequestBody tokenPart = RequestBody.create(MediaType.parse("text/plain"), token);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("photoImg", "photo.jpeg", RequestBody.create(MediaType.parse("image/jpeg"), file));
            Call<PostRequestResult> call;
            if (latitude != null && longitude != null) {
                RequestBody latitudePart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude.doubleValue()));
                RequestBody longitudePart = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude.doubleValue()));
                call = apiClient.uploadPhoto(tokenPart, latitudePart, longitudePart, filePart);
            } else {
                call = apiClient.uploadPhoto(tokenPart, filePart);
            }

            call.enqueue(new Callback<PostRequestResult>() {
                @Override
                public void onResponse(Call<PostRequestResult> call, Response<PostRequestResult> response) {
                    if (response.isSuccessful()) {
                        LogWriter.writeMessage("Photo uploaded sucessfully");
                        listener.postRequestCompleted(response.body());
                    } else {
                        try {
                            LogWriter.writeError("Error returned by server when trying to upload photo: " + response.errorBody().string());
                        } catch (IOException e) {
                            LogWriter.writeError("Failed getting response from error body", e);
                        }
                        listener.postRequestCompleted(null);
                    }
                }

                @Override
                public void onFailure(Call<PostRequestResult> call, Throwable t) {
                    LogWriter.writeError("Error while trying to upload photo", t);
                    listener.postRequestCompleted(null);
                }
            });
        } catch (Exception e) {
            LogWriter.writeError("Error while trying to upload photo", e);
            listener.postRequestCompleted(null);
        }
    }

    public void addLikeToPhoto(String photoUUID, Double longitude, Double latitude, String token, final OnPostRequestCompleted listener) {
        Call<PostRequestResult> call;
        if (latitude != null && longitude != null) {
            call = apiClient.likePhoto(new PhotoLikeBody(token, photoUUID, latitude, longitude));
        } else {
            call = apiClient.likePhoto(new PhotoLikeBody(token, photoUUID));
        }
        call.enqueue(new Callback<PostRequestResult>() {
            @Override
            public void onResponse(Call<PostRequestResult> call, Response<PostRequestResult> response) {
                if (response.isSuccessful()) {
                    LogWriter.writeMessage("Photo liked sucessfully");
                    listener.postRequestCompleted(response.body());
                } else {
                    try {
                        LogWriter.writeError("Error returned by server when trying to add like to a photo: " + response.errorBody().string());
                    } catch (IOException e) {
                        LogWriter.writeError("Failed getting response from error body", e);
                    }
                    listener.postRequestCompleted(null);
                }
            }

            @Override
            public void onFailure(Call<PostRequestResult> call, Throwable t) {
                LogWriter.writeError("Error while trying to add like to a photo", t);
                listener.postRequestCompleted(null);
            }
        });
    }

    public void reportPhoto(String photoUUID, String reportCode, String token, final OnPostRequestCompleted listener) {
        Call<PostRequestResult> call = apiClient.reportPhoto(new PhotoReportBody(token, photoUUID, reportCode));
        call.enqueue(new Callback<PostRequestResult>() {
            @Override
            public void onResponse(Call<PostRequestResult> call, Response<PostRequestResult> response) {
                if (response.isSuccessful()) {
                    LogWriter.writeMessage("Photo reported sucessfully");
                    listener.postRequestCompleted(response.body());
                } else {
                    try {
                        LogWriter.writeError("Error returned by server when trying to report a photo: " + response.errorBody().string());
                    } catch (IOException e) {
                        LogWriter.writeError("Failed getting response from error body", e);
                    }
                    listener.postRequestCompleted(null);
                }
            }

            @Override
            public void onFailure(Call<PostRequestResult> call, Throwable t) {
                LogWriter.writeError("Error while trying to report a photo", t);
                listener.postRequestCompleted(null);
            }
        });
    }

    public interface OnPostRequestCompleted {
        void postRequestCompleted(PostRequestResult result);
    }
}
