package com.efimerum.efimerum.manager.deeplinks;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.appinvite.AppInviteInvitationResult;
import com.google.android.gms.appinvite.AppInviteReferral;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;

public class DeepLinksManager {

    public interface CheckDeepLinkLaunchListerner {
        void checkResult(boolean deepLinkLaunched, String data);
    }

    public void checkDeepLinkLaunch(AppCompatActivity activity, final CheckDeepLinkLaunchListerner listener) {
        final GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity)
                .enableAutoManage(activity, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        listener.checkResult(false, null);
                    }
                })
                .addApi(AppInvite.API)
                .build();

        AppInvite.AppInviteApi.getInvitation(googleApiClient, activity, false).setResultCallback(
            new ResultCallback<AppInviteInvitationResult>() {
                @Override
                public void onResult(@NonNull AppInviteInvitationResult result) {
                    if (result.getStatus().isSuccess()) {
                        // Extract deep link from Intent
                        Intent intent = result.getInvitationIntent();
                        String deepLink = AppInviteReferral.getDeepLink(intent);
                        // Handle the deep link
                        int lastSlash = deepLink.lastIndexOf("/");
                        String photoKey = deepLink.substring(lastSlash + 1);
                        listener.checkResult(true, photoKey);
                    } else {
                        listener.checkResult(false, null);
                    }

                    googleApiClient.disconnect();
                }
            });
    }
}
