package com.efimerum.efimerum.manager.firebase;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FBImageData {
    public int height;
    public String url;
    public int width;
}
