package com.efimerum.efimerum.manager.firebase;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;

@IgnoreExtraProperties
public class FBPhoto {
    @Exclude
    public String uuid;
    public long creationDate;
    public long expirationDate;
    public double latitude;
    public double longitude;
    public long numOfLikes;
    public String owner;
    public String md5;
    public String randomString;
    public String sha1;
    public String sha256;
    public String dynamicLink;
    public FBImageData imageData;
    public FBImageData thumbnailData;
    public HashMap<String, HashMap<String, String>> labels;
}
