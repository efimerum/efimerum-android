package com.efimerum.efimerum.manager.communication;


import com.efimerum.efimerum.manager.db.DatabaseManager;
import com.efimerum.efimerum.manager.firebase.FBPhoto;
import com.efimerum.efimerum.manager.firebase.FirebaseHelper;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.mappers.PhotoFBPhotoMapper;

import java.util.ArrayList;
import java.util.List;

public class ServerEventsManager {
    private List<PhotosEventsListener> photosListeners;
    private List<TagsEventsListener> tagsListeners;
    private FirebaseHelper.FirebaseEventListener<FBPhoto> photosEventListener;
    private FirebaseHelper.FirebaseEventListener<String> tagsEventListener;

    public ServerEventsManager() {
        photosListeners = new ArrayList<>();
        tagsListeners = new ArrayList<>();

        photosEventListener = new FirebaseHelper.FirebaseEventListener<FBPhoto>() {
            @Override
            public void itmeAdded(FBPhoto fbPhoto) {
                final Photo photo = new PhotoFBPhotoMapper().map(fbPhoto);
                DatabaseManager.getInstance().insertOrUpdatePhoto(photo, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (PhotosEventsListener listener : photosListeners) {
                                listener.photoAdded(photo);
                            }
                        }
                    }
                });
            }

            @Override
            public void itemDeleted(FBPhoto fbPhoto) {
                final Photo photo = new PhotoFBPhotoMapper().map(fbPhoto);
                DatabaseManager.getInstance().deletePhoto(photo, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (PhotosEventsListener listener : photosListeners) {
                                listener.photoRemoved(photo);
                            }
                        }
                    }
                });
            }

            @Override
            public void itemModified(FBPhoto fbPhoto) {
                final Photo photo = new PhotoFBPhotoMapper().map(fbPhoto);
                DatabaseManager.getInstance().insertOrUpdatePhoto(photo, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (PhotosEventsListener listener : photosListeners) {
                                listener.photoModified(photo);
                            }
                        }
                    }
                });
            }
        };

        tagsEventListener = new FirebaseHelper.FirebaseEventListener<String>() {
            @Override
            public void itmeAdded(String item) {
                final Tag tag = new Tag(item);
                DatabaseManager.getInstance().insertOrUpdateTag(tag, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (TagsEventsListener listener : tagsListeners) {
                                listener.tagAdded(tag);
                            }
                        }
                    }
                });
            }

            @Override
            public void itemDeleted(String item) {
                final Tag tag = new Tag(item);
                DatabaseManager.getInstance().deleteTag(tag, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (TagsEventsListener listener : tagsListeners) {
                                listener.tagRemoved(tag);
                            }
                        }
                    }
                });
            }

            @Override
            public void itemModified(String item) {
                final Tag tag = new Tag(item);
                DatabaseManager.getInstance().insertOrUpdateTag(tag, new DatabaseManager.UpdateOperationResponse() {
                    @Override
                    public void result(boolean succeded) {
                        if (succeded) {
                            for (TagsEventsListener listener : tagsListeners) {
                                listener.tagModified(tag);
                            }
                        }
                    }
                });
            }
        };
    }

    public void addPhotosListener(PhotosEventsListener listener) {
        photosListeners.add(listener);
    }

    public void removePhotosListener(PhotosEventsListener listener) {
        photosListeners.remove(listener);
    }

    public void addTagsListener(TagsEventsListener listener) {
        tagsListeners.add(listener);
    }

    public void removeTagsListener(TagsEventsListener listener) {
        tagsListeners.remove(listener);
    }

    public FirebaseHelper.FirebaseEventListener<FBPhoto> getPhotosEventListener() {
        return photosEventListener;
    }

    public FirebaseHelper.FirebaseEventListener<String> getTagsEventListener() {
        return tagsEventListener;
    }

    public interface PhotosEventsListener {
        void photoAdded(Photo photo);
        void photoRemoved(Photo photo);
        void photoModified(Photo photo);
    }

    public interface TagsEventsListener {
        void tagAdded(Tag tag);
        void tagRemoved(Tag tag);
        void tagModified(Tag tag);
    }
}
