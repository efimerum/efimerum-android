package com.efimerum.efimerum.manager.location;


import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.efimerum.efimerum.util.LogWriter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.lang.ref.WeakReference;

import static com.google.android.gms.location.LocationServices.FusedLocationApi;

public class LocationManager implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private GoogleApiClient googleApiClient;
    private GetLocationListener listener;
    private LocationRequest locationRequest;

    private WeakReference<Activity> activity;

    public LocationManager(Activity activity) {
        this.activity = new WeakReference<>(activity);

        googleApiClient = new GoogleApiClient.Builder(this.activity.get())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public boolean isGetLocationAllowed() {
        return ContextCompat.checkSelfPermission(activity.get(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestGetLocationPermission(int requestCode) {
        ActivityCompat.requestPermissions(activity.get(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
    }

    public void getLocation(GetLocationListener listener) {
        this.listener = listener;
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            FusedLocationApi.flushLocations(googleApiClient);
            Location lastLocation = FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation == null) {
                startLocationUpdates();
            } else {
                LogWriter.writeMessage("Last location got successfully");
                if (this.listener != null) {
                    listener.getLocationSucceded(lastLocation.getLongitude(), lastLocation.getLatitude());
                }

                googleApiClient.disconnect();
            }
        } catch (SecurityException ex) {
            LogWriter.writeError("Unable to get lastLocation", ex);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        LogWriter.writeError("Unable to get lastLocation: ConnectionSuspended");
        if (this.listener != null) {
            listener.getLocationFailed();
        }

        stopLocationUpdates();
        googleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        LogWriter.writeError("Unable to get lastLocation: ConnectionFailed");
        if (this.listener != null) {
            listener.getLocationFailed();
        }

        stopLocationUpdates();
        googleApiClient.disconnect();
    }

    private void startLocationUpdates() throws SecurityException {
        FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    private void stopLocationUpdates() {
        FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        LogWriter.writeMessage("New location retrieved by device");

        if (listener != null) {
            listener.getLocationSucceded(location.getLongitude(), location.getLatitude());
        }

        stopLocationUpdates();
        googleApiClient.disconnect();
    }

    public interface GetLocationListener {
        void getLocationSucceded(double longitude, double latitude);
        void getLocationFailed();
    }
}
