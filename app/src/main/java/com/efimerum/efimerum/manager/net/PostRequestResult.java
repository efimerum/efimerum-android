package com.efimerum.efimerum.manager.net;


import com.google.gson.annotations.SerializedName;

public class PostRequestResult {
    @SerializedName("success") private boolean succeeded;
    @SerializedName("error") private String errorMessage;
    @SerializedName("data") private String resultData;

    public boolean isSucceeded() {
        return succeeded;
    }

    public void setSucceeded(boolean succeeded) {
        this.succeeded = succeeded;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResultData() {
        return resultData;
    }

    public void setResultData(String resultData) {
        this.resultData = resultData;
    }
}
