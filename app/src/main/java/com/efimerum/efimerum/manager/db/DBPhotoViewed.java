package com.efimerum.efimerum.manager.db;


import io.realm.RealmObject;
import io.realm.annotations.Index;

public class DBPhotoViewed extends RealmObject {
    private String photoId;
    @Index private String userId;
    private boolean liked;

    public DBPhotoViewed() {
    }

    public DBPhotoViewed(String photoId, String userId, boolean liked) {
        this.photoId = photoId;
        this.userId = userId;
        this.liked = liked;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }
}
