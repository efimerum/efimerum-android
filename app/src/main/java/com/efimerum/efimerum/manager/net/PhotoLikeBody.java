package com.efimerum.efimerum.manager.net;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoLikeBody {
    @SerializedName("photoKey") @Expose
    private String photoUuid;

    @SerializedName("idToken") @Expose
    private String token;

    @SerializedName("latitude") @Expose
    private Double latitude;

    @SerializedName("longitude") @Expose
    private Double longitude;

    public PhotoLikeBody(String token, String photoUuid) {
        this.token = token;
        this.photoUuid = photoUuid;
    }

    public PhotoLikeBody(String token, String photoUuid, Double latitude, Double longitude) {
        this.token = token;
        this.photoUuid = photoUuid;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
