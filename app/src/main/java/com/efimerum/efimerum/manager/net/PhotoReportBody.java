package com.efimerum.efimerum.manager.net;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PhotoReportBody {
    @SerializedName("photoKey") @Expose
    private String photoUuid;

    @SerializedName("idToken") @Expose
    private String token;

    @SerializedName("reportCode") @Expose
    private String code;

    public PhotoReportBody(String token, String photoUuid, String code) {
        this.token = token;
        this.photoUuid = photoUuid;
        this.code = code;
    }
}
