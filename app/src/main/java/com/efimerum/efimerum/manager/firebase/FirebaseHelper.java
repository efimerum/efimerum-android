package com.efimerum.efimerum.manager.firebase;


import android.net.Uri;
import android.support.annotation.NonNull;

import com.efimerum.efimerum.util.LogWriter;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class FirebaseHelper {

    private static final String USERS_NODE = "users";
    private static final String TAGS_NODE = "labels";
    private static final String PHOTOS_NODE = "photos";
    private static final String PHOTOS_BY_TAG_NODE = "photosByLabel";
    private static final String USER_PROFILE_PHOTOS_NODE = "profile_images";
    private static final String PHOTOS_LIKED_BY_USER_NODE = "photosLikedByUser";

    private static FirebaseHelper sharedInstance;
    private FirebaseAuth auth;
    private FirebaseDatabase database;
    private FirebaseStorage storage;
    private boolean databaseInitialized = false;

    private FirebaseHelper() {
        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
    }

    public synchronized static FirebaseHelper getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new FirebaseHelper();
        }

        return sharedInstance;
    }

    public void getCurrentUser(final FirebaseUserListener listener) {
        FirebaseUser user = auth.getCurrentUser();
        if (user == null) {
            auth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        LogWriter.writeError("Error when trying to login as anonymous user", task.getException());
                        listener.getUser(false, null);
                    } else {
                        LogWriter.writeMessage("Succesfully logged as anonymous user");
                        firebaseUserToFBUser(task.getResult().getUser(), listener);
                    }
                }
            });
        } else {
            LogWriter.writeMessage("Using previously logged user");
            firebaseUserToFBUser(user, listener);
        }
    }

    public void login(String email, String password, final FirebaseUserListener listener) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    LogWriter.writeError("Error when trying to login", task.getException());
                    listener.getUser(false, null);
                } else {
                    LogWriter.writeMessage("Succesfully logged in");
                    updateFCMToken(task.getResult().getUser().getUid(), FirebaseInstanceId.getInstance().getToken(), listener);
                }
            }
        });
    }

    public void logout(final FBUser user, final FirebaseUserListener listener) {
        auth.signOut();
        updateFCMToken(user.uuid, null, listener);
    }

    public void getToken(final FirebaseTokenListener listener) {
        FirebaseUser user = auth.getCurrentUser();
        if (user != null) {
            user.getToken(false).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (!task.isSuccessful()) {
                        LogWriter.writeError("Error when trying to get user token", task.getException());
                        listener.getToken(false, null);
                    } else {
                        LogWriter.writeMessage("Succesfully got token form current user");
                        listener.getToken(true, task.getResult().getToken());
                    }
                }
            });
        } else {
            LogWriter.writeError("Error when trying to get user token, no user logged in");
            listener.getToken(false, null);
        }
    }

    public void registerUser(final FBUser user, String password, final FirebaseUserListener listener) {
        FirebaseUser firebaseUser = auth.getCurrentUser();
        if (firebaseUser != null) {
            AuthCredential credential = EmailAuthProvider.getCredential(user.email, password);
            firebaseUser.linkWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull final Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        LogWriter.writeError("Error when trying to link registered user with an anonymous user", task.getException());
                        listener.getUser(false, null);
                    } else {
                        LogWriter.writeMessage("Succesfully linked registered user with an anonymous user");
                        user.uuid = task.getResult().getUser().getUid();
                        user.fcmToken = FirebaseInstanceId.getInstance().getToken();

                        if (user.profileImageURL != null) {
                            Uri file = Uri.parse(user.profileImageURL);
                            uploadFile(USER_PROFILE_PHOTOS_NODE, file, new FirebaseUploadFileListener() {
                                @Override
                                public void getUploadedFile(boolean succeded, Uri uploadedFile) {
                                    if (succeded) {
                                        user.profileImageURL = uploadedFile.toString();
                                        updateUserInfo(user, listener);
                                    } else {
                                        listener.getUser(false, null);
                                    }
                                }
                            });
                        } else {
                            updateUserInfo(user, listener);
                        }
                    }
                }
            });
        } else {
            auth.createUserWithEmailAndPassword(user.email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        LogWriter.writeError("Error when trying to register new user", task.getException());
                        listener.getUser(false, null);
                    } else {
                        LogWriter.writeMessage("Succesfully registered new user");
                        user.uuid = task.getResult().getUser().getUid();
                        user.fcmToken = FirebaseInstanceId.getInstance().getToken();
                        updateUserInfo(user, listener);
                    }
                }
            });
        }
    }

    public void updateAvatar(final FBUser user, String profileImageURL, final FirebaseUserListener listener) {
        Uri file = Uri.parse(profileImageURL);
        uploadFile(USER_PROFILE_PHOTOS_NODE, file, new FirebaseUploadFileListener() {
            @Override
            public void getUploadedFile(boolean succeded, Uri uploadedFile) {
                if (succeded) {
                    user.profileImageURL = uploadedFile.toString();
                    updateUserInfo(user, listener);
                } else {
                    listener.getUser(false, null);
                }
            }
        });
    }

    public void updateUserInfo(final FBUser user, final FirebaseUserListener listener) {
        database.getReference(USERS_NODE).child(user.uuid).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    LogWriter.writeError("Error when trying to save new user information", databaseError.toException());
                    listener.getUser(false, null);
                } else {
                    LogWriter.writeMessage("Succesfully saved user information");
                    listener.getUser(true, user);
                }
            }
        });
    }

    public void updateFCMToken(final String uuid, final String fcmToken, final FirebaseUserListener listener) {
        database.getReference(USERS_NODE).child(uuid).child("fcmToken").setValue(
                fcmToken, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            LogWriter.writeError("Error when trying to save FCM token", databaseError.toException());
                            listener.getUser(false, null);
                        } else {
                            LogWriter.writeMessage("Succesfully saved FCM token");
                            getUserInformation(uuid, listener);
                        }
                    }
                });
    }

    public void initializeFirebaseDatabase(String language,
                                           FirebaseEventListener<FBPhoto> photosListener,
                                           FirebaseEventListener<String> tagsListener,
                                           final FirebaseDatabaseInitializationListener listener) {

        subscribeToPhotosNode(photosListener);
        subscribeToTagsNode(language, tagsListener);

        database.getReference(TAGS_NODE).child(language).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LogWriter.writeMessage("Succesfully retrieved tags collection");
                HashMap<String, String> tags = (HashMap<String, String>) dataSnapshot.getValue();
                final List<String> tagsList = new ArrayList<>(tags.keySet());

                database.getReference(PHOTOS_NODE).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        LogWriter.writeMessage("Succesfully retrieved photos collection");
                        List<FBPhoto> photos = new ArrayList<>();

                        for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                            try {
                                FBPhoto photo = childDataSnapshot.getValue(FBPhoto.class);
                                photo.uuid = childDataSnapshot.getKey();
                                photos.add(photo);
                            } catch (Exception ex) {
                                LogWriter.writeError("Error when trying to parse photo from server: " + ex.getMessage());
                            }
                        }

                        listener.initializationResult(true, photos, tagsList);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        LogWriter.writeError("Error when trying to get all photos", databaseError.toException());
                        listener.initializationResult(false, null, null);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LogWriter.writeError("Error when trying to get all tags", databaseError.toException());
                listener.initializationResult(false, null, null);
            }
        });
    }

    public void getPhotosByLocation(String tag, String language, double latitude, double longitude, double radius, final FirebaseNearPhotosListener listener) {
        DatabaseReference reference;
        if (tag == null) {
            reference = database.getReference(PHOTOS_NODE);
        } else {
            reference = database.getReference(PHOTOS_BY_TAG_NODE).child(language).child(tag);
        }

        final List<String> keys = new ArrayList<>();

        GeoFire geoFire = new GeoFire(reference);
        final GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(latitude, longitude), radius);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                keys.add(key);
            }

            @Override
            public void onKeyExited(String key) {
            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
            }

            @Override
            public void onGeoQueryReady() {
                geoQuery.removeAllListeners();
                listener.nearPhotos(true, keys);
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                LogWriter.writeError("Error when trying to get near photos", error.toException());
                listener.nearPhotos(false, null);
            }
        });
    }

    public void getUserInformation(final String uuid, final FirebaseUserListener listener) {
        database.getReference(USERS_NODE).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LogWriter.writeMessage("Succesfully retrieved user information");
                FBUser user = dataSnapshot.getValue(FBUser.class);
                user.uuid = uuid;
                listener.getUser(true, user);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LogWriter.writeError("Error when trying to get user information", databaseError.toException());
                listener.getUser(false, null);
            }
        });
    }

    private void uploadFile(String destinationNode, Uri fileUri, final FirebaseUploadFileListener listener) {
        String fileName = UUID.randomUUID().toString();
        StorageReference storageRef = storage.getReference().child(destinationNode).child(fileName + ".jpg");
        storageRef.putFile(fileUri).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                LogWriter.writeError("Error when trying to upload file", e);
                listener.getUploadedFile(false, null);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                LogWriter.writeMessage("Succesfully linked registered user with an anonymous user");
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                listener.getUploadedFile(true, downloadUrl);
            }
        });
    }

    private void subscribeToPhotosNode(final FirebaseEventListener<FBPhoto> listener) {
        DatabaseReference reference = database.getReference(PHOTOS_NODE);
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    if (databaseInitialized) {
                        LogWriter.writeMessage("onChildAdded: photo " + dataSnapshot.getKey());
                        FBPhoto photo = dataSnapshot.getValue(FBPhoto.class);
                        photo.uuid = dataSnapshot.getKey();
                        listener.itmeAdded(photo);
                    }
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                try {
                    LogWriter.writeMessage("onChildChanged: photo " + dataSnapshot.getKey());
                    FBPhoto photo = dataSnapshot.getValue(FBPhoto.class);
                    photo.uuid = dataSnapshot.getKey();
                    listener.itemModified(photo);
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                try {
                    LogWriter.writeMessage("onChildRemoved: photo " + dataSnapshot.getKey());
                    FBPhoto photo = dataSnapshot.getValue(FBPhoto.class);
                    photo.uuid = dataSnapshot.getKey();
                    listener.itemDeleted(photo);
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                LogWriter.writeMessage("onChildMoved: photo " + dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LogWriter.writeError("onCancelled: photo ", databaseError.toException());
            }
        });
    }

    private void subscribeToTagsNode(String language, final FirebaseEventListener<String> listener) {
        DatabaseReference reference = database.getReference(TAGS_NODE).child(language);
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                try {
                    if (databaseInitialized) {
                        LogWriter.writeMessage("onChildAdded: tag " + dataSnapshot.getKey());
                        listener.itmeAdded(dataSnapshot.getKey());
                    }
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                try {
                    LogWriter.writeMessage("onChildChanged: tag " + dataSnapshot.getKey());
                    listener.itemModified(dataSnapshot.getKey());
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                try {
                    LogWriter.writeMessage("onChildRemoved: tag " + dataSnapshot.getKey());
                    listener.itemDeleted(dataSnapshot.getKey());
                } catch (Exception ex) {
                    LogWriter.writeError("onChildAdded Error", ex);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                LogWriter.writeMessage("onChildMoved: tag " + dataSnapshot.getKey());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LogWriter.writeError("onCancelled: tag ", databaseError.toException());
            }
        });
    }

    private void firebaseUserToFBUser(FirebaseUser firebaseUser, FirebaseUserListener listener) {
        if (firebaseUser.isAnonymous()) {
            listener.getUser(true, new FBUser(firebaseUser.getUid()));
        } else {
            getUserInformation(firebaseUser.getUid(), listener);
        }
    }

    public void getLikedPhotos(final String uuid, final FirebasePhotosListener listener) {
        database.getReference(PHOTOS_LIKED_BY_USER_NODE).child(uuid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                LogWriter.writeMessage("Succesfully retrieved photos liked by user[" + uuid + "] collection");
                List<FBPhoto> photos = new ArrayList<>();

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    try {
                        FBPhoto photo = childDataSnapshot.getValue(FBPhoto.class);
                        photo.uuid = childDataSnapshot.getKey();
                        photos.add(photo);
                    } catch (Exception ex) {
                        LogWriter.writeError("Error when trying to parse photo liked by user[" + uuid + "] from server: " + ex.getMessage());
                    }
                }

                listener.getPhotos(true, photos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                LogWriter.writeError("Error when trying to get photos liked by user[" + uuid + "]", databaseError.toException());
                listener.getPhotos(false, null);
            }
        });
    }

    public interface FirebaseUserListener {
        void getUser(boolean succeded, FBUser fbUser);
    }

    public interface FirebaseTokenListener {
        void getToken(boolean succeded, String token);
    }

    public interface FirebaseDatabaseInitializationListener {
        void initializationResult(boolean succeded, List<FBPhoto> photos, List<String> tags);
    }

    public interface FirebaseNearPhotosListener {
        void nearPhotos(boolean succeded, List<String> photos);
    }

    public interface FirebaseUploadFileListener {
        void getUploadedFile(boolean succeded, Uri uploadedFile);

    }

    public interface FirebaseEventListener<T> {
        void itmeAdded(T item);

        void itemDeleted(T item);

        void itemModified(T Item);

    }

    public interface FirebasePhotosListener {
        void getPhotos(boolean succeded, List<FBPhoto> photos);
    }
}
