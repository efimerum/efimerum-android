package com.efimerum.efimerum.manager.db;


import android.content.Context;

import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.mappers.PhotoDBPhotoMapper;
import com.efimerum.efimerum.model.mappers.TagDBTagMapper;
import com.efimerum.efimerum.util.LogWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class DatabaseManager {

    private static DatabaseManager sharedInstance;
    private Realm realm;

    private DatabaseManager() {
        realm = Realm.getDefaultInstance();
    }

    public synchronized static DatabaseManager getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new DatabaseManager();
        }

        return sharedInstance;
    }

    public static void initializeDatabase(Context context) {
        Realm.init(context);
    }

    public void removeAllData(final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(DBPhoto.class);
                realm.delete(DBTag.class);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("Data removed succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to remove all data", error);
                response.result(false);
            }
        });
    }

    public void insertInitialData(final Photos photos, final List<Tag> tags, final UpdateOperationResponse response) {

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (int i = 0; i < photos.size(); i++) {
                    Photo photo = photos.get(i);
                    DBPhoto dbPhoto = new PhotoDBPhotoMapper().photoToDBPhoto(photo);
                    realm.copyToRealmOrUpdate(dbPhoto);
                }

                for (int i = 0; i < tags.size(); i++) {
                    Tag tag = tags.get(i);
                    DBTag dbTag = new TagDBTagMapper().tagToDBTag(tag);
                    realm.copyToRealmOrUpdate(dbTag);
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("Initial data inserted into database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to insert initial data into database", error);
                response.result(false);
            }
        });
    }

    public Photos getAllPhotos() {
        return getAllPhotos(null, null, null, null, false, false);
    }

    public Photos getAllPhotos(String userId) {
        return getAllPhotos(userId, null, null, null, false, false);
    }

    public Photos getAllPhotos(String userId, Tag tag) {
        return getAllPhotos(userId, tag, null, null, false, false);
    }

    public Photos getAllPhotos(String userId, Tag tag, List<String> filterPhotos) {
        return getAllPhotos(userId, tag, filterPhotos, null, false, false);
    }

    public Photos getAllPhotos(String userId, Tag tag, String sortField, boolean descending) {
        return getAllPhotos(userId, tag, null, sortField, descending, false);
    }

    public Photos getAllPhotos(String userId, Tag tag, String sortField, boolean descending, boolean onlyMyPhotos) {
        return getAllPhotos(userId, tag, null, sortField, descending, onlyMyPhotos);
    }

    private Photos getAllPhotos(String userId, Tag tag, List<String> filterPhotos, String sortBy, boolean descending, boolean onlyMyPhotos) {
        Photos returnValue = null;

        try {
            List<Photo> photos = new ArrayList<>();
            RealmQuery<DBPhoto> query;
            if (userId != null) {
                if (onlyMyPhotos) {
                    query = realm.where(DBPhoto.class).equalTo("owner", userId);
                } else {
                    List<String> ids = new ArrayList<>();
                    RealmResults<DBPhotoViewed> photoVieweds = realm.where(DBPhotoViewed.class).equalTo("userId", userId).findAll();
                    for (DBPhotoViewed photoViewed : photoVieweds) {
                        ids.add(photoViewed.getPhotoId());
                    }

                    if (ids.size() > 0) {
                        String[] array = ids.toArray(new String[0]);
                        query = realm.where(DBPhoto.class).not().in("id", array);
                    } else {
                        query = realm.where(DBPhoto.class);
                    }
                }
            } else {
                query = realm.where(DBPhoto.class);
            }

            if (tag != null) {
                query = query.equalTo("tags.name", tag.getText(), Case.INSENSITIVE);
            }

            if (filterPhotos != null) {
                if (filterPhotos.size() > 0) {
                    query = query.in("id", filterPhotos.toArray(new String[0]));
                } else {
                    return Photos.build(photos);
                }
            }

            List<String> sortFields = new ArrayList<>(Arrays.asList("sort_field"));
            List<Sort> sortCriteria = new ArrayList<>(Arrays.asList(Sort.ASCENDING));
            if (sortBy != null) {
                sortFields.add(0, sortBy);
                sortCriteria.add(0, descending ? Sort.DESCENDING : Sort.ASCENDING);
            }

            RealmResults<DBPhoto> result = query.findAllSorted(sortFields.toArray(new String[0]), sortCriteria.toArray(new Sort[0]));
            for (DBPhoto photo : result) {
                photos.add(new PhotoDBPhotoMapper().dbPhotoToPhoto(photo));
            }

            returnValue = Photos.build(photos);
        } catch (Exception ex) {
            LogWriter.writeError("Error when trying to get all photos from database", ex);
        }

        return returnValue;
    }

    public Photo getPhoto(final String photoUUID) {
        Photo returnValue = null;

        try {
            DBPhoto dbPhoto = realm.where(DBPhoto.class).equalTo("id", photoUUID).findFirst();
            returnValue = new PhotoDBPhotoMapper().dbPhotoToPhoto(dbPhoto);
        } catch (Exception ex) {
            LogWriter.writeError("Error when trying to get a photo [" + photoUUID + "] from database", ex);
        }

        return returnValue;
    }

    public void addPhotoViewed(final String photoUUID, final String userId, final boolean liked, final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBPhotoViewed photoViewed = new DBPhotoViewed(photoUUID, userId, liked);
                realm.copyToRealm(photoViewed);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("Photo viewed inserted into database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to insert a photo viewed into database", error);
                response.result(false);
            }
        });
    }

    public List<Tag> getTags() {
        List<Tag> tags = new ArrayList<>();

        try {
            RealmResults<DBTag> results = realm.where(DBTag.class).findAll();
            for (DBTag dbTag : results) {
                Tag tag = new TagDBTagMapper().dbTagToTag(dbTag);
                tags.add(tag);
            }

        } catch (Exception ex) {
            tags = new ArrayList<>();
            LogWriter.writeError("Error when trying to get tags from database", ex);
        }

        return tags;
    }

    public void insertOrUpdatePhoto(final Photo photo, final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBPhoto dbPhoto = new PhotoDBPhotoMapper().photoToDBPhoto(photo);
                realm.copyToRealmOrUpdate(dbPhoto);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("New photo inserted into database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to insert a new photo into database", error);
                response.result(false);
            }
        });
    }

    public void deletePhoto(final Photo photo, final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBPhoto dbPhoto = realm.where(DBPhoto.class).equalTo("id", photo.getUuid()).findFirst();
                if (dbPhoto != null) {
                    dbPhoto.deleteFromRealm();
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("Photo deleted from database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to delete a photo from database", error);
                response.result(false);
            }
        });
    }

    public void insertOrUpdateTag(final Tag tag, final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBTag dbTag = new TagDBTagMapper().tagToDBTag(tag);
                realm.copyToRealmOrUpdate(dbTag);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("New tag inserted into database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to insert a new tag into database", error);
                response.result(false);
            }
        });
    }

    public void deleteTag(final Tag tag, final UpdateOperationResponse response) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                DBTag dbTag = realm.where(DBTag.class).equalTo("name", tag.getText()).findFirst();
                if (dbTag != null) {
                    dbTag.deleteFromRealm();
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                LogWriter.writeMessage("Tag deleted from database succesfully");
                response.result(true);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                LogWriter.writeError("Error when trying to delete a tag from database", error);
                response.result(false);
            }
        });
    }

    public interface UpdateOperationResponse {
        void result(boolean succeded);
    }
}
