package com.efimerum.efimerum.manager.net;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIClient {
    @Multipart
    @POST("photos")
    Call<PostRequestResult> uploadPhoto(@Part("idToken") RequestBody token,
                                        @Part MultipartBody.Part file);

    @Multipart
    @POST("photos")
    Call<PostRequestResult> uploadPhoto(@Part("idToken") RequestBody token,
                                        @Part("latitude") RequestBody latitude,
                                        @Part("longitude") RequestBody longitude,
                                        @Part MultipartBody.Part file);

    @POST("likes")
    Call<PostRequestResult> likePhoto(@Body PhotoLikeBody body);

    @POST("reportPhoto")
    Call<PostRequestResult> reportPhoto(@Body PhotoReportBody body);
}
