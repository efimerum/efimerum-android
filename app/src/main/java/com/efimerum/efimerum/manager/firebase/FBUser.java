package com.efimerum.efimerum.manager.firebase;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class FBUser {
    @Exclude
    public String uuid;
    public String name;
    public String email;
    public String profileImageURL;
    public String fcmToken;
    public boolean likeNotificationEnabled;

    public FBUser() {
    }

    public FBUser(String uuid) {
        this.uuid = uuid;
    }
}
