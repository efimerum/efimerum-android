package com.efimerum.efimerum.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.adapter.AutoCompleteTagsAdapter;
import com.efimerum.efimerum.fragment.PhotoGridFragment;
import com.efimerum.efimerum.interactor.GetPhotosInteractor;
import com.efimerum.efimerum.interactor.GetTagsInteractor;
import com.efimerum.efimerum.interactor.UploadPhotoInteractor;
import com.efimerum.efimerum.manager.communication.ServerEventsManager;
import com.efimerum.efimerum.manager.deeplinks.DeepLinksManager;
import com.efimerum.efimerum.manager.location.LocationManager;
import com.efimerum.efimerum.model.BubbleItem;
import com.efimerum.efimerum.model.Filter;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.navigator.Navigator;
import com.efimerum.efimerum.util.CommonFunctions;
import com.efimerum.efimerum.util.Constants;
import com.efimerum.efimerum.view.BubbleLinearLayout;
import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.completefab.CompleteFABView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import toan.android.floatingactionmenu.FloatingActionsMenu;

import static android.view.View.VISIBLE;
import static com.efimerum.efimerum.manager.images.ImagesManager.captureImage;
import static com.efimerum.efimerum.util.Constants.SHOW_PHOTO_REQUEST_CODE;

public class MainActivity extends AppCompatActivity implements ServerEventsManager.TagsEventsListener, PhotoGridFragment.PhotoGridFragmentEventsListener {

    private static final int DELAY = 600;
    private static final int CAPTURE_IMAGE_REQUEST_CODE = 2;
    private static final int PERMISSIONS_REQUEST_CAMERA = 3;
    private static final int VIEW_PROFILE_ACTION = 4;
    private static final int TAKE_PHOTO_ACTION = 5;
    private static final int PERMISSIONS_REQUEST_LOCATION_FOR_PHOTO = 6;
    private static final int PERMISSIONS_REQUEST_LOCATION_FOR_FILTER = 7;

    @BindView(R.id.main_activity_camera_fab)
    FloatingActionButton fabCamera;
    @BindView(R.id.main_activity_camera_fab_animation)
    FABProgressCircle fabCameraAnimation;
    @BindView(R.id.main_activity_filters_fab)
    FloatingActionsMenu fabFilters;
    @BindView(R.id.main_activity_search_fab)
    FloatingActionButton fabSearch;
    @BindView(R.id.main_activity_logo_image)
    ImageButton btnLogo;
    @BindView(R.id.main_activity_close_search_btn)
    ImageView btnCloseSearch;
    @BindView(R.id.main_activity_search_et)
    AutoCompleteTextView etSearch;
    @BindView(R.id.main_activity_shadow_layout)
    RelativeLayout shadowLayout;
    @BindView(R.id.main_activity_toolbar_layout)
    FABToolbarLayout toolbarLayout;
    @BindView(R.id.main_activity_header_layout)
    LinearLayout headerLayout;
    @BindView(R.id.main_activity_most_liked_filter)
    toan.android.floatingactionmenu.FloatingActionButton mostLikedFilter;
    @BindView(R.id.main_activity_nearest_filter)
    toan.android.floatingactionmenu.FloatingActionButton nearestFilter;
    @BindView(R.id.main_activity_about_to_die_filter)
    toan.android.floatingactionmenu.FloatingActionButton aboutToDieFilter;
    @BindView(R.id.main_activity_more_life_filter)
    toan.android.floatingactionmenu.FloatingActionButton moreLifeFilter;

    private List<Tag> tags;
    private Photos photos;
    private boolean isFirstScrollEvent = true;
    private boolean isSerching = false;
    private boolean isUploadingPhoto = false;
    private String tempPicturePath = null;
    private PhotoGridFragment photosFragment;
    private LocationManager locationManager;
    private View.OnClickListener filtersOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            fabFilters.collapse();

            toan.android.floatingactionmenu.FloatingActionButton button = (toan.android.floatingactionmenu.FloatingActionButton) view;

            Filter filter = new Filter();
            filter.setId(button.getId());
            filter.setText(button.getTitle());

            addBubble(filter);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        CommonFunctions.setTranslucentStatusBar(getWindow());

        this.locationManager = new LocationManager(this);

        new GetTagsInteractor().execute(new GetTagsInteractor.GetTagsInteractorResponse() {
            @Override
            public void response(List<Tag> tags) {
                MainActivity.this.tags = tags;

                new GetPhotosInteractor().execute(MainActivity.this, null, null, null, null, new GetPhotosInteractor.GetPhotosInteractorResponse() {
                    @Override
                    public void getPhotos(Photos photos) {
                        MainActivity.this.photos = photos;
                        initializeLayout();

                        EfimerumApp app = (EfimerumApp) getApplication();
                        app.getEventsManager().addTagsListener(MainActivity.this);

                        new DeepLinksManager().checkDeepLinkLaunch(MainActivity.this, new DeepLinksManager.CheckDeepLinkLaunchListerner() {
                            @Override
                            public void checkResult(boolean deepLinkLaunched, String data) {
                                if (deepLinkLaunched && data != null) {
                                    Navigator.navigateFromMainActivityToPhotoActivityWithPhotoKey(MainActivity.this, data);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        EfimerumApp app = (EfimerumApp) getApplication();
        app.getEventsManager().removeTagsListener(this);

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.LOGIN_REQUEST_CODE: {
                    int action = data.getIntExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, -1);
                    switch (action) {
                        case VIEW_PROFILE_ACTION: {
                            Navigator.navigateFromMainActivityToUserProfileActivity(MainActivity.this);
                        }
                        break;

                        case TAKE_PHOTO_ACTION: {
                            refreshPhotos(true);
                            takePhotoOrRequestPermission();
                        }
                    }
                }
                break;

                case CAPTURE_IMAGE_REQUEST_CODE: {
                    if (tempPicturePath != null) {
                        if (locationManager.isGetLocationAllowed()) {
                            getLocationForPhotoUpload(tempPicturePath);
                        } else {
                            locationManager.requestGetLocationPermission(PERMISSIONS_REQUEST_LOCATION_FOR_PHOTO);
                        }
                    }
                }
                break;

                case SHOW_PHOTO_REQUEST_CODE: {
                    refreshPhotos(false);
                }
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    tempPicturePath = captureImage(this, CAPTURE_IMAGE_REQUEST_CODE);
                }
                break;
            }

            case PERMISSIONS_REQUEST_LOCATION_FOR_PHOTO: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationForPhotoUpload(tempPicturePath);
                } else {
                    uploadPhoto(tempPicturePath, null, null);
                }
                break;
            }

            case PERMISSIONS_REQUEST_LOCATION_FOR_FILTER: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationManager.getLocation(new LocationManager.GetLocationListener() {
                        @Override
                        public void getLocationSucceded(double longitude, double latitude) {
                            requestPhotos(latitude, longitude);
                        }

                        @Override
                        public void getLocationFailed() {
                            requestPhotos(null, null);
                        }
                    });
                } else {
                    requestPhotos(null, null);
                }
                break;
            }
        }
    }

    private void initializeLayout() {

        CommonFunctions.changeEditTextBaseLineColor(etSearch, Color.TRANSPARENT);

        shadowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // filter touches when underlying view is Obscured by this view.
            }
        });

        photosFragment = PhotoGridFragment.newInstance(this.photos);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_activity_photos_fragment, photosFragment).commit();

        fabFilters.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                shadowLayout.setVisibility(VISIBLE);
                shadowLayout.bringToFront();
                fabFilters.bringToFront();
            }

            @Override
            public void onMenuCollapsed() {
                shadowLayout.setVisibility(View.GONE);
            }
        });

        fabSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSerching = true;
                CommonFunctions.changeFloatingActionButtonBackgroundColor(MainActivity.this, fabSearch, R.color.search_bar_color);
                toolbarLayout.show();
                changeButtonsState(View.GONE, false);

                executeDelayedRunnable(new Runnable() {
                    public void run() {
                        CommonFunctions.showKeyboard(MainActivity.this, etSearch);
                    }
                });
            }
        });

        btnCloseSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeSearchToolBar();
            }
        });

        initializeSearchEditText();

        mostLikedFilter.setOnClickListener(filtersOnClickListener);
        nearestFilter.setOnClickListener(filtersOnClickListener);
        aboutToDieFilter.setOnClickListener(filtersOnClickListener);
        moreLifeFilter.setOnClickListener(filtersOnClickListener);

        fabCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isUploadingPhoto) {
                    EfimerumApp app = (EfimerumApp) getApplication();
                    User user = app.getCurrentUser();
                    if (user == null || user.isAnonymous()) {
                        Navigator.navigateFromMainActivityToLoginActivity(MainActivity.this, TAKE_PHOTO_ACTION);
                    } else {
                        takePhotoOrRequestPermission();
                    }
                }
            }
        });

        btnLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                User user = ((EfimerumApp) getApplication()).getCurrentUser();
                if (user != null && !user.isAnonymous()) {
                    Navigator.navigateFromMainActivityToUserProfileActivity(MainActivity.this);
                } else {
                    Navigator.navigateFromMainActivityToLoginActivity(MainActivity.this, VIEW_PROFILE_ACTION);
                }
            }
        });

        // Hack to get displaying finalIcon. It is a library bug
        fabCameraAnimation.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ImageView imgView = (ImageView) v.findViewById(R.id.completeFabIcon);
                if ((imgView != null) && (imgView.getScaleType() != ImageView.ScaleType.CENTER_INSIDE)) {
                    imgView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
            }
        });
    }

    private void takePhotoOrRequestPermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_CAMERA);
        } else {
            tempPicturePath = captureImage(MainActivity.this, CAPTURE_IMAGE_REQUEST_CODE);
        }
    }

    private void initializeSearchEditText() {
        AutoCompleteTagsAdapter adapter = new AutoCompleteTagsAdapter(MainActivity.this,
                android.R.layout.simple_dropdown_item_1line, this.tags);
        etSearch.setAdapter(adapter);

        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tag selectedTag = (Tag) adapterView.getItemAtPosition(i);
                closeSearchToolBar();

                addBubble(selectedTag);
            }
        });
    }

    private void changeButtonsState(int state, boolean changeSearchState) {
        fabCameraAnimation.setVisibility(state);
        fabFilters.setVisibility(state);

        for (int i = 1; i < headerLayout.getChildCount(); i++) {
            View view = headerLayout.getChildAt(i);
            view.setVisibility(state);
        }

        if (changeSearchState) {
            fabSearch.setVisibility(state);
        }
    }

    private void closeSearchToolBar() {
        isSerching = false;
        toolbarLayout.hide();
        etSearch.setText("");
        CommonFunctions.hideKeyboard(MainActivity.this);

        executeDelayedRunnable(new Runnable() {
            public void run() {
                CommonFunctions.changeFloatingActionButtonBackgroundColor(MainActivity.this, fabSearch, R.color.search_fab_color);
                changeButtonsState(VISIBLE, true);
            }
        });
    }

    private void executeDelayedRunnable(Runnable runable) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(runable, DELAY);
    }

    private void addBubble(BubbleItem item) {
        BubbleLinearLayout bubbleLinearLayout = getBubbleByClass(item.getClass());
        if (bubbleLinearLayout != null) {
            headerLayout.removeView(bubbleLinearLayout);
        }

        final BubbleLinearLayout layout = new BubbleLinearLayout(MainActivity.this, item);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                headerLayout.removeView(layout);
                refreshPhotos(true);
            }
        });
        headerLayout.addView(layout);
        refreshPhotos(true);
    }

    private void refreshPhotos(boolean scrollToTop) {
        if (scrollToTop) {
            isFirstScrollEvent = true;
            photosFragment.scrollToTop();
        }

        Filter appliedFiler = getApliedFilter();
        if (appliedFiler != null && appliedFiler.getId() == R.id.main_activity_nearest_filter) {
            if (locationManager.isGetLocationAllowed()) {
                locationManager.getLocation(new LocationManager.GetLocationListener() {
                    @Override
                    public void getLocationSucceded(double longitude, double latitude) {
                        requestPhotos(latitude, longitude);
                    }

                    @Override
                    public void getLocationFailed() {
                        requestPhotos(null, null);
                    }
                });
            } else {
                locationManager.requestGetLocationPermission(PERMISSIONS_REQUEST_LOCATION_FOR_FILTER);
            }
        } else {
            requestPhotos(null, null);
        }
    }

    private void requestPhotos(Double latitude, Double longitude) {
        new GetPhotosInteractor().execute(MainActivity.this, getApliedTag(), getApliedFilter(), latitude, longitude, new GetPhotosInteractor.GetPhotosInteractorResponse() {
            @Override
            public void getPhotos(Photos photos) {
                MainActivity.this.photos = photos;
                photosFragment.setPhotos(photos);
            }
        });
    }

    private void refreshTags() {
        new GetTagsInteractor().execute(new GetTagsInteractor.GetTagsInteractorResponse() {
            @Override
            public void response(List<Tag> tags) {
                MainActivity.this.tags = tags;
            }
        });
    }

    private Tag getApliedTag() {
        BubbleLinearLayout bubbleLinearLayout = getBubbleByClass(Tag.class);
        if (bubbleLinearLayout != null) {
            return (Tag) bubbleLinearLayout.getItem();
        }

        return null;
    }

    private Filter getApliedFilter() {
        BubbleLinearLayout bubbleLinearLayout = getBubbleByClass(Filter.class);
        if (bubbleLinearLayout != null) {
            return (Filter) bubbleLinearLayout.getItem();
        }

        return null;
    }

    private BubbleLinearLayout getBubbleByClass(Class className) {
        for (int i = 0; i < headerLayout.getChildCount(); i++) {
            View view = headerLayout.getChildAt(i);
            if (view instanceof BubbleLinearLayout && ((BubbleLinearLayout) view).getItem().getClass() == className) {
                return (BubbleLinearLayout) view;
            }
        }

        return null;
    }

    private void getLocationForPhotoUpload(final String imagePath) {
        locationManager.getLocation(new LocationManager.GetLocationListener() {
            @Override
            public void getLocationSucceded(double longitude, double latitude) {
                uploadPhoto(imagePath, latitude, longitude);
            }

            @Override
            public void getLocationFailed() {
                uploadPhoto(imagePath, null, null);
            }
        });
    }

    private void uploadPhoto(String imagePath, Double latitude, Double longitude) {
        // Hack to get displaying finalIcon. It is a library bug
        for (int i = 0; i < fabCameraAnimation.getChildCount(); i++) {
            if (fabCameraAnimation.getChildAt(i) instanceof CompleteFABView) {
                fabCameraAnimation.removeViewAt(i);
                break;
            }
        }

        fabCameraAnimation.show();

        isUploadingPhoto = true;
        new UploadPhotoInteractor().execute(MainActivity.this, imagePath, latitude, longitude, new UploadPhotoInteractor.UploadPhotoInteractorResponse() {
            @Override
            public void response(boolean succeded) {
                isUploadingPhoto = false;
                if (!succeded) {
                    fabCameraAnimation.hide();
                    CommonFunctions.showErrorDialog(MainActivity.this, R.string.main_activity_upload_photo_error_title, R.string.main_activity_upload_photo_error_text);
                } else {
                    fabCameraAnimation.beginFinalAnimation();
                }
            }
        });
    }

    @Override
    public void tagAdded(Tag tag) {
        refreshTags();
    }

    @Override
    public void tagRemoved(Tag tag) {
        refreshTags();
    }

    @Override
    public void tagModified(Tag tag) {
        refreshTags();
    }

    @Override
    public void onGridScrolling() {
        if (!isSerching) {
            changeButtonsState(VISIBLE, true);
        }
    }

    @Override
    public void onScrolled() {
        if (!isSerching) {
            if (!isFirstScrollEvent) {
                changeButtonsState(View.GONE, true);
            } else {
                isFirstScrollEvent = false;
            }
        }
    }

    @Override
    public void onPhotoClicked(Photo photo, int position) {
        Navigator.navigateFromMainActivityToPhotoActivity(MainActivity.this, photos, position);
    }
}