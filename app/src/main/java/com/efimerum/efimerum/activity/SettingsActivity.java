package com.efimerum.efimerum.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.interactor.LogoutInteractor;
import com.efimerum.efimerum.interactor.UpdateLikeNotificationSettingsInteractor;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.navigator.Navigator;
import com.efimerum.efimerum.util.CommonFunctions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.settings_activity_switch_like_notification)
    SwitchCompat switchLikeNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        CommonFunctions.setTranslucentStatusBar(getWindow());

        initializeLayout();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout: {
                EfimerumApp app = (EfimerumApp) getApplication();
                new LogoutInteractor().execute(app.getCurrentUser(), new LogoutInteractor.LogoutInteractorResponse() {
                    @Override
                    public void response(User user) {
                        EfimerumApp app = (EfimerumApp) getApplication();
                        app.setCurrentUser(user);
                        Toast.makeText(SettingsActivity.this, "Logged out", Toast.LENGTH_SHORT).show();
                        Navigator.navigateFromSettingsActivityToMainActivity(SettingsActivity.this);
                    }
                });
                return true;
            }

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void initializeLayout() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_settings);
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setTitle("  " + getResources().getString(R.string.settings_activity_title_text));
        setSupportActionBar(toolbar);

        EfimerumApp app = (EfimerumApp) getApplication();
        switchLikeNotifications.setChecked(app.getCurrentUser().isLikeNotificationEnabled());
        switchLikeNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EfimerumApp app = (EfimerumApp) getApplication();
                new UpdateLikeNotificationSettingsInteractor().execute(app.getCurrentUser(), isChecked,
                        new UpdateLikeNotificationSettingsInteractor.UpdateLikeNotificationSettingsInteractorResponse() {
                            @Override
                            public void response(User user) {
                                EfimerumApp app = (EfimerumApp) getApplication();
                                app.setCurrentUser(user);
                                Toast.makeText(SettingsActivity.this,
                                        user.isLikeNotificationEnabled() ? "Like notification enabled!!!" : "Like notification disabled!!!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }
}
