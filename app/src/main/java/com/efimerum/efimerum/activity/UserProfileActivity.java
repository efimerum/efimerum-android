package com.efimerum.efimerum.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.adapter.UserProfileFragmentPagerAdapter;
import com.efimerum.efimerum.fragment.EditAvatarFragment;
import com.efimerum.efimerum.fragment.EditUserNameFragment;
import com.efimerum.efimerum.fragment.PhotoGridFragment;
import com.efimerum.efimerum.interactor.GetPhotosInteractor;
import com.efimerum.efimerum.interactor.UpdateUserNameInteractor;
import com.efimerum.efimerum.interactor.UploadAvatarInteractor;
import com.efimerum.efimerum.manager.images.ImagesManager;
import com.efimerum.efimerum.model.Filter;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.navigator.Navigator;
import com.efimerum.efimerum.util.CommonFunctions;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.efimerum.efimerum.manager.images.ImagesManager.captureImage;

public class UserProfileActivity extends AppCompatActivity implements
        PhotoGridFragment.PhotoGridFragmentEventsListener,
        EditAvatarFragment.EditAvatarDialogListener,
        EditUserNameFragment.EditUserNameDialogListener {

    private static final int CAPTURE_IMAGE_REQUEST_CODE = 2;
    private static final int PERMISSIONS_REQUEST_CAMERA = 3;
    private static final int SELECT_IMAGE_REQUEST_CODE = 4;
    private static final int PERMISSIONS_REQUEST_GALLERY = 5;
    private static final String SELECTED_PANEL_INDEX_KEY = "TAB_LAYOUT_INDEX";

    @BindView(R.id.user_profile_activity_settings_image)
    ImageView settingsImageView;
    @BindView(R.id.user_profile_activity_avatar_image)
    ImageView avatarImageView;
    @BindView(R.id.user_profile_activity_name_tv)
    TextView nameTextView;
    @BindView(R.id.user_profile_activity_email_tv)
    TextView emailTextView;
    @BindView(R.id.user_profile_activity_tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.user_profile_activity_viewpager)
    ViewPager viewPager;
    @BindView(R.id.user_profile_activity_edit_name_image)
    ImageView editNameImageView;

    private UserProfileFragmentPagerAdapter userProfileFragmentPagerAdapter;
    private String tempPicturePath = null;
    private Photos myPostedPhotos;
    private Photos myLikedPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);

        CommonFunctions.setTranslucentStatusBar(getWindow());

        initializeLayout();

        Filter filter = new Filter();
        filter.setId(GetPhotosInteractor.ONLY_MY_POSTED_PHOTOS);
        new GetPhotosInteractor().execute(this, null, filter, null, null, new GetPhotosInteractor.GetPhotosInteractorResponse() {
            @Override
            public void getPhotos(Photos photos) {
                UserProfileActivity.this.myPostedPhotos = photos;
                Filter filter = new Filter();
                filter.setId(GetPhotosInteractor.ONLY_MY_LIKED_PHOTOS);
                new GetPhotosInteractor().execute(UserProfileActivity.this, null, filter, null, null, new GetPhotosInteractor.GetPhotosInteractorResponse() {
                    @Override
                    public void getPhotos(Photos photos) {
                        UserProfileActivity.this.myLikedPhotos = photos;
                        initializePhotosLayout(UserProfileActivity.this.myPostedPhotos, UserProfileActivity.this.myLikedPhotos);
                    }
                });
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(SELECTED_PANEL_INDEX_KEY, tabLayout.getSelectedTabPosition());
    }

    private void initializeLayout() {
        User currentUser = ((EfimerumApp) getApplication()).getCurrentUser();
        updateUserInformation(currentUser);

        avatarImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditAvatarFragment.newInstance().show(getFragmentManager(), null);
            }
        });

        settingsImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.navigateFromUserProfileActivityToSettingsActivity(UserProfileActivity.this);
            }
        });

        editNameImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditUserNameFragment.newInstance().show(getFragmentManager(), null);
            }
        });
    }

    private void updateUserInformation(User user) {
        if (user.getPhotoUrl() != null) {
            Picasso.with(this)
                    .load(user.getPhotoUrl())
                    .into(avatarImageView);
        } else {
            avatarImageView.setImageResource(R.drawable.flame);
        }

        nameTextView.setText(user.getName());
        emailTextView.setText(user.getEmail());
    }

    private void initializePhotosLayout(Photos myPostedPhotos, Photos myLikedPhotos) {
        viewPager = (ViewPager) findViewById(R.id.user_profile_activity_viewpager);
        userProfileFragmentPagerAdapter = new UserProfileFragmentPagerAdapter(getSupportFragmentManager(), myPostedPhotos, myLikedPhotos);
        viewPager.setAdapter(userProfileFragmentPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.user_profile_activity_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onGridScrolling() {

    }

    @Override
    public void onScrolled() {

    }

    @Override
    public void onPhotoClicked(Photo photo, int position) {
        if (tabLayout.getSelectedTabPosition() == 0) {
            Navigator.navigateFromUserProfileActivityToPhotoActivity(UserProfileActivity.this, myPostedPhotos, position);
        } else if (tabLayout.getSelectedTabPosition() == 1) {
            Navigator.navigateFromUserProfileActivityToPhotoActivity(UserProfileActivity.this, myLikedPhotos, position);
        }
    }

    @Override
    public void editResult(int resultCode) {
        if (resultCode == EditAvatarFragment.RESULT_CAMERA) {
            takePhotoOrRequestPermission();
        } else if (resultCode == EditAvatarFragment.RESULT_GALLERY) {
            selectImageOrRequestPermission();
        }
    }

    private void selectImageOrRequestPermission() {
        if (ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserProfileActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_GALLERY);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_IMAGE_REQUEST_CODE);
        }
    }

    private void takePhotoOrRequestPermission() {
        if (ContextCompat.checkSelfPermission(UserProfileActivity.this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserProfileActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_CAMERA);
        } else {
            tempPicturePath = captureImage(UserProfileActivity.this, CAPTURE_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    tempPicturePath = captureImage(this, CAPTURE_IMAGE_REQUEST_CODE);
                }
                break;
            }

            case PERMISSIONS_REQUEST_GALLERY: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_IMAGE_REQUEST_CODE);
                }
                break;
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_IMAGE_REQUEST_CODE: {
                    if (tempPicturePath != null) {
                        uploadPhoto(tempPicturePath);
                    }
                }
                break;

                case SELECT_IMAGE_REQUEST_CODE: {
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        String selectedImagePath = ImagesManager.getPath(this, selectedImageUri);
                        uploadPhoto(selectedImagePath);
                    }
                }
                break;
            }
        }
    }

    private void uploadPhoto(String imagePath) {
        new UploadAvatarInteractor().execute(
                UserProfileActivity.this,
                ((EfimerumApp) getApplication()).getCurrentUser(),
                imagePath,
                new UploadAvatarInteractor.UploadAvatarInteractorResponse() {
                    @Override
                    public void response(User user) {
                        if (user == null) {
                            CommonFunctions.showErrorDialog(UserProfileActivity.this, R.string.main_activity_upload_photo_error_title, R.string.main_activity_upload_photo_error_text);
                        } else {
                            ((EfimerumApp) getApplication()).setCurrentUser(user);
                            updateUserInformation(user);
                            Toast.makeText(UserProfileActivity.this,
                                    "User\'s photo updated!!!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void editResult(String newName, int resultCode) {
        if (resultCode == EditUserNameFragment.RESULT_OK) {
            User user = ((EfimerumApp) getApplication()).getCurrentUser();
            new UpdateUserNameInteractor().execute(user, newName, new UpdateUserNameInteractor.UpdateUserNameInteractorResponse() {
                @Override
                public void response(User user) {
                    EfimerumApp app = (EfimerumApp) getApplication();
                    app.setCurrentUser(user);
                    updateUserInformation(user);
                    Toast.makeText(UserProfileActivity.this,
                            "User\'s name updated!!!",
                            Toast.LENGTH_SHORT).show();
                }
            });

        }
    }
}