package com.efimerum.efimerum.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.fragment.ReportFragment;
import com.efimerum.efimerum.interactor.GetPhotoInteractor;
import com.efimerum.efimerum.interactor.LikePhotoInteractor;
import com.efimerum.efimerum.interactor.ReportPhotoInteractor;
import com.efimerum.efimerum.interactor.SetPhotoViewedInteractor;
import com.efimerum.efimerum.manager.location.LocationManager;
import com.efimerum.efimerum.model.BanReason;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.navigator.Navigator;
import com.efimerum.efimerum.util.CommonFunctions;
import com.efimerum.efimerum.util.Constants;
import com.efimerum.efimerum.util.PhotoEventsListener;
import com.efimerum.efimerum.view.PhotoView;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PhotoActivity extends AppCompatActivity implements PhotoEventsListener, ReportFragment.PhotoBanDialogListener {
    private static final String CURRENT_INDEX_KEY = "CURRENT_INDEX";
    private static final String PHOTO_VIEWED_KEY = "PHOTO_VIEWED";
    private static final String SHOW_LIKE_DISLIKE_KEY = "SHOW_LIKE_DISLIKE_KEY";
    private static final int BANNED_ACTION_LOGIN_RESPONSE = 0;
    private static final int LIKED_ACTION_LOGIN_RESPONSE = 1;
    private static final int DISLIKED_ACTION_LOGIN_RESPONSE = 2;
    private static final int PERMISSIONS_REQUEST_LOCATION = 3;

    @BindView(R.id.photo_activity_swipe_view)
    SwipePlaceHolderView swipePlaceHolder;

    private Photos photos;
    private int currentIndex = -1;
    private boolean photoViewed = false;
    private LocationManager locationManager;
    private String showLikeDislike = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        CommonFunctions.setTranslucentStatusBar(this.getWindow());

        ButterKnife.bind(this);

        this.locationManager = new LocationManager(this);

        if (savedInstanceState != null) {
            currentIndex = savedInstanceState.getInt(CURRENT_INDEX_KEY, -1);
            photoViewed = savedInstanceState.getBoolean(PHOTO_VIEWED_KEY, false);
            showLikeDislike = savedInstanceState.getString(SHOW_LIKE_DISLIKE_KEY, null);
        }

        Intent intent = getIntent();

        if (currentIndex == -1) {
            currentIndex = intent.getIntExtra(Constants.INTENT_KEY_PHOTO_INDEX, -1);
        }

        if (showLikeDislike == null) {
            showLikeDislike = intent.getStringExtra(Constants.INTENT_SHOW_LIKE_DISLIKE);
        }

        if (intent.hasExtra(Constants.INTENT_BACKEND_KEY_PHOTO_UUID)) {
            String photoKey = getIntent().getStringExtra(Constants.INTENT_BACKEND_KEY_PHOTO_UUID);
            new GetPhotoInteractor().execute(photoKey, new GetPhotoInteractor.GetPhotoInteractorResponse() {
                @Override
                public void getPhoto(Photo photo) {
                    photos = Photos.build(photo);
                    currentIndex = 0;
                    initializeLayout();
                }
            });

        } else {
            photos = (Photos) getIntent().getSerializableExtra(Constants.INTENT_KEY_PHOTOS);
            initializeLayout();
        }
    }

    @Override
    public void onBackPressed() {
        if (photoViewed) {
            setResult(RESULT_OK);
        }

        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCATION: {
                final Photo photo = photos.get(currentIndex - 1);
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocationForLike(photo);
                } else {
                    registerLikeToPhoto(photo, null, null);
                }
                break;
            }
        }
    }

    @Override
    public void photoViewed(int position, final Photo photo, boolean liked) {
        currentIndex = position + 1;
        photoViewed = true;

        EfimerumApp app = (EfimerumApp) getApplication();
        User user = app.getCurrentUser();
        if (user == null || user.isAnonymous()) {
            Navigator.navigateFromPhotoActivityToLoginActivity(this, photo, liked ? LIKED_ACTION_LOGIN_RESPONSE : DISLIKED_ACTION_LOGIN_RESPONSE);
        } else if (liked) {
            requestLocationPermissionForLike(photo);
        } else {
            registerPhotoViewed(photo);
        }
    }

    @Override
    public void photoClicked(Photo photo) {
        Navigator.navigateFromPhotoActivityToMapActivity(this, photo);
    }

    @Override
    public void photoBanned(Photo photo) {
        EfimerumApp app = (EfimerumApp) getApplication();
        User user = app.getCurrentUser();
        if (user == null || user.isAnonymous()) {
            Navigator.navigateFromPhotoActivityToLoginActivity(this, photo, BANNED_ACTION_LOGIN_RESPONSE);
        } else {
            ReportFragment.newInstance(photo).show(getFragmentManager(), null);
        }
    }

    @Override
    public void photoShared(Photo photo) {
        if (photo.getShareURL() != null) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_TEXT, photo.getShareURL());
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.photo_activity_share_subject));
            startActivity(Intent.createChooser(intent, getString(R.string.photo_activity_share_text)));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(CURRENT_INDEX_KEY, currentIndex);
        outState.putBoolean(PHOTO_VIEWED_KEY, photoViewed);
        outState.putString(SHOW_LIKE_DISLIKE_KEY, showLikeDislike);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == Constants.LOGIN_REQUEST_CODE && data != null && data.hasExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_DATA)) {
            Photo photo = (Photo) data.getSerializableExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_DATA);
            switch (data.getIntExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, -1)) {
                case BANNED_ACTION_LOGIN_RESPONSE: {
                    ReportFragment.newInstance(photo).show(getFragmentManager(), null);
                }
                break;
                case LIKED_ACTION_LOGIN_RESPONSE: {
                    requestLocationPermissionForLike(photo);
                }
                break;
                case DISLIKED_ACTION_LOGIN_RESPONSE: {
                    registerPhotoViewed(photo);
                }
                break;
            }
        }
    }

    @Override
    public void BanResult(Photo photo, BanReason reason) {
        if (reason != null) {
            new ReportPhotoInteractor().execute(this, photo.getUuid(), reason.getCode(), new ReportPhotoInteractor.ReportPhotoInteractorResponse() {
                @Override
                public void response(boolean succeded) {
                }
            });

            swipePlaceHolder.doSwipe(false);
        }
    }

    private void initializeLayout() {
        if (photos != null && currentIndex > -1 && photos.size() > currentIndex) {

            swipePlaceHolder.getBuilder()
                    .setDisplayViewCount(5)
                    .setSwipeDecor(new SwipeDecor());

            EfimerumApp app = (EfimerumApp) getApplication();
            User user = app.getCurrentUser();

            for (int i = currentIndex; i < photos.size(); i++) {
                Photo photo = photos.get(i);
                if (!photo.isDeleted()) {
                    boolean bFlag = true;
                    if (user.getUid().equals(photo.getOwner())) {
                        bFlag = false;
                    } else {
                        if (showLikeDislike != null) {
                            bFlag = showLikeDislike.equals("true");
                        }
                    }
                    PhotoView view = new PhotoView(this, photo, swipePlaceHolder, i, bFlag);
                    view.setListener(this);
                    swipePlaceHolder.addView(view);
                }
            }
        }
    }

    private void requestLocationPermissionForLike(final Photo photo) {
        if (locationManager.isGetLocationAllowed()) {
            getLocationForLike(photo);
        } else {
            locationManager.requestGetLocationPermission(PERMISSIONS_REQUEST_LOCATION);
        }
    }

    private void getLocationForLike(final Photo photo) {
        locationManager.getLocation(new LocationManager.GetLocationListener() {
            @Override
            public void getLocationSucceded(double longitude, double latitude) {
                registerLikeToPhoto(photo, latitude, longitude);
            }

            @Override
            public void getLocationFailed() {
                registerLikeToPhoto(photo, null, null);
            }
        });
    }

    private void registerLikeToPhoto(final Photo photo, final Double latitude, final Double longitude) {
        new SetPhotoViewedInteractor().execute(this, photo.getUuid(), false, new SetPhotoViewedInteractor.SetPhotoViewedInteractorResponse() {
            @Override
            public void response(boolean succeded) {
                new LikePhotoInteractor().execute(PhotoActivity.this, photo.getUuid(), longitude, latitude, new LikePhotoInteractor.LikePhotoInteractorResponse() {
                    @Override
                    public void response(boolean succeded) {
                    }
                });

                if (currentIndex >= photos.size()) {
                    onBackPressed();
                }
            }
        });
    }

    private void registerPhotoViewed(Photo photo) {
        new SetPhotoViewedInteractor().execute(this, photo.getUuid(), false, new SetPhotoViewedInteractor.SetPhotoViewedInteractorResponse() {
            @Override
            public void response(boolean succeded) {
                if (currentIndex >= photos.size()) {
                    onBackPressed();
                }
            }
        });
    }
}
