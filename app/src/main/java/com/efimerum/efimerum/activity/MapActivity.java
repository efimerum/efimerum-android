package com.efimerum.efimerum.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.GeolocationPermissions;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.interactor.GetPhotoOwnerInteractor;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.util.CommonFunctions;
import com.efimerum.efimerum.util.Constants;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends AppCompatActivity {

    private static final String UNKNOWN_OWNER_NAME = "Unknown";

    @BindView(R.id.map_activity_webview) WebView webView;
    @BindView(R.id.map_activity_author_text) TextView authorTextView;
    @BindView(R.id.map_activity_number_of_likes_text) TextView likesTextView;
    @BindView(R.id.map_activity_expiration_date_text) TextView expirationTextView;
    @BindView(R.id.map_activity_tags_text) TextView tagsTextView;
    @BindView(R.id.map_activity_photo_info_container) LinearLayout photoInfoContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        ButterKnife.bind(this);

        CommonFunctions.setTranslucentStatusBar(getWindow());

        Intent intent = getIntent();
        final Photo photo = (Photo) intent.getSerializableExtra(Constants.INTENT_KEY_PHOTO);
        if (photo != null) {
            String photoUUID = photo.getUuid();
            loadWebView(photoUUID);

            if (photo.getOwner() != null) {
                new GetPhotoOwnerInteractor().execute(photo.getOwner(), new GetPhotoOwnerInteractor.GetPhotoOwnerInteractorResponse() {
                    @Override
                    public void response(String ownerName) {
                        fillPhotoInformation(photo, ownerName != null ? ownerName : UNKNOWN_OWNER_NAME);
                    }
                });
            } else {
                fillPhotoInformation(photo, UNKNOWN_OWNER_NAME);
            }
        }
    }

    private void fillPhotoInformation(Photo photo, String owner) {
        authorTextView.setText(getString(R.string.map_activity_author_text, owner));

        likesTextView.setText(getString(R.string.map_activity_likes_text, photo.getLikes()));

        PrettyTime prettyTime = new PrettyTime(new Locale("en"));
        expirationTextView.setText(getString(R.string.map_activity_expiration_text, prettyTime.format(photo.getExpires())));

        StringBuilder tagsBuilder = new StringBuilder("  ");
        for (int i = 0; i < photo.getTags().size(); i++) {
            String tagName = photo.getTags().get(i).getText();
            tagsBuilder.append(i == 0 ? tagName : " - " + tagName);
        }
        tagsTextView.setText(tagsBuilder.toString());

        photoInfoContainer.setVisibility(View.VISIBLE);
    }

    private void loadWebView(String photoUUID) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setGeolocationEnabled(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onGeolocationPermissionsShowPrompt(final String origin, final GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, true);
            }
        });

        String url = getString(R.string.config_map_url) + (photoUUID != null ? photoUUID : "");
        webView.loadUrl(url);
    }
}
