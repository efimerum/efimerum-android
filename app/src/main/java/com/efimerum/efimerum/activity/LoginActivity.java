package com.efimerum.efimerum.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.interactor.LoginInteractor;
import com.efimerum.efimerum.interactor.RegisterUserInteractor;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.util.CommonFunctions;
import com.efimerum.efimerum.util.Constants;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private static final int PASSWORD_MINIMUM_LENGTH = 6;
    private static final String SELECTED_PANEL_INDEX_KEY = "TAB_LAYOUT_INDEX";

    @BindView(R.id.login_activity_login_button) Button loginButton;
    @BindView(R.id.login_activity_cancel_button) Button cancelButton;
    @BindView(R.id.login_activity_name_et) EditText nameEditText;
    @BindView(R.id.login_activity_email_et) EditText emailEditText;
    @BindView(R.id.login_activity_password_et) EditText passwordEditText;
    @BindView(R.id.login_activity_logo_image) ImageView logoImageView;
    @BindView(R.id.login_activity_tab_layout) TabLayout tabLayout;
    @BindView(R.id.login_activity_recover_password_tv) TextView recoverPasswordTextView;

    private Serializable responseData;
    private int responseAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent != null) {
            responseAction = intent.getIntExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, -1);
            responseData = intent.getSerializableExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_DATA);
        }

        int tabIndex = savedInstanceState != null ? savedInstanceState.getInt(SELECTED_PANEL_INDEX_KEY, 0) : 0;
        TabLayout.Tab tab = tabLayout.getTabAt(tabIndex);
        tab.select();

        initializeLayout();
        updateFormState();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(SELECTED_PANEL_INDEX_KEY, tabLayout.getSelectedTabPosition());
    }

    private void initializeLayout() {
        recoverPasswordTextView.setPaintFlags(recoverPasswordTextView.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        recoverPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateFormState();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    if (isRegisterForm()) {
                        final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "", getString(R.string.login_activity_register_progress_message), true);
                        User user = new User(emailEditText.getText().toString(), nameEditText.getText().toString(), null);
                        new RegisterUserInteractor().execute(user, passwordEditText.getText().toString(), new RegisterUserInteractor.RegisterUserInteractorResponse() {
                            @Override
                            public void response(User user) {
                                progressDialog.dismiss();
                                if (user != null) {
                                    setUserAndFinish(user);
                                } else {
                                    CommonFunctions.showErrorDialog(LoginActivity.this, R.string.login_activity_register_error_title, R.string.login_activity_register_error_text);
                                }
                            }
                        });

                    } else {
                        final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "", getString(R.string.login_activity_login_progress_message), true);
                        new LoginInteractor().execute(emailEditText.getText().toString(),
                                passwordEditText.getText().toString(),
                                new LoginInteractor.LoginInteractorResponse() {
                                    @Override
                                    public void response(User user) {
                                        progressDialog.dismiss();
                                        if (user != null) {
                                            setUserAndFinish(user);
                                        } else {
                                            CommonFunctions.showErrorDialog(LoginActivity.this, R.string.login_activity_login_error_title, R.string.login_activity_login_error_text);
                                        }
                                    }
                        });
                    }
                }

            }
        });
    }

    private boolean validateForm() {
        nameEditText.setError(null);
        emailEditText.setError(null);
        passwordEditText.setError(null);

        if (isRegisterForm()) {
            String nameValue = nameEditText.getText().toString();
            if (nameValue.length() < 1) {
                nameEditText.setError(getString(R.string.login_activity_name_error_text));
                return false;
            }
        }

        String emailValue = emailEditText.getText().toString();
        if (emailValue.length() < 1 || !Patterns.EMAIL_ADDRESS.matcher(emailValue).matches()) {
            emailEditText.setError(getString(R.string.login_activity_name_email_text));
            return false;
        }

        String passwordValue = passwordEditText.getText().toString();
        if (passwordValue.length() < PASSWORD_MINIMUM_LENGTH) {
            passwordEditText.setError(String.format(getString(R.string.login_activity_password_error_text), PASSWORD_MINIMUM_LENGTH));
            return false;
        }

        return true;
    }

    private void updateFormState() {
        if (isRegisterForm()) {
            nameEditText.setVisibility(View.VISIBLE);
            recoverPasswordTextView.setVisibility(View.GONE);
            loginButton.setText(R.string.login_activity_register_text);
            nameEditText.requestFocus();
        } else {
            nameEditText.setVisibility(View.GONE);
            recoverPasswordTextView.setVisibility(View.VISIBLE);
            loginButton.setText(R.string.login_activity_login_text);
            emailEditText.requestFocus();
        }
    }

    private void setUserAndFinish(User user) {
        EfimerumApp app  = (EfimerumApp) getApplication();
        app.setCurrentUser(user);

        Intent intent = new Intent();
        if (responseData != null) {
            intent.putExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_DATA, responseData);
        }
        if (responseAction != -1) {
            intent.putExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, responseAction);
        }

        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean isRegisterForm() {
        return tabLayout.getSelectedTabPosition() == 1;
    }
}
