package com.efimerum.efimerum.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.interactor.DownloadInitialDataInteractor;
import com.efimerum.efimerum.interactor.LoadUserInteractor;
import com.efimerum.efimerum.interactor.StorageInitialDataInteractor;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.model.User;
import com.efimerum.efimerum.navigator.Navigator;
import com.efimerum.efimerum.util.CommonFunctions;

import java.util.List;


public class SplashScreenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        CommonFunctions.setTranslucentStatusBar(getWindow());

        new LoadUserInteractor().execute(new LoadUserInteractor.LoadUserInteractorResponse() {
            @Override
            public void response(User user) {
                EfimerumApp app = (EfimerumApp) getApplication();
                app.setCurrentUser(user);

                new DownloadInitialDataInteractor().execute(app.getEventsManager(), new DownloadInitialDataInteractor.DownloadInitialDataInteractorResponse() {
                    @Override
                    public void response(Photos photos, List<Tag> tags) {
                        if (photos == null) {
                            CommonFunctions.showErrorDialog(SplashScreenActivity.this, R.string.app_name, R.string.splash_screen_activity_photos_error);
                            return;
                        }

                        new StorageInitialDataInteractor().execute(photos, tags, new StorageInitialDataInteractor.StorageInitialDataInteractorResponse() {
                            @Override
                            public void response(boolean succeded) {
                                if (succeded) {
                                    Navigator.navigateFromSplashScreenActivityToMainActivity(SplashScreenActivity.this);
                                } else {
                                    CommonFunctions.showErrorDialog(SplashScreenActivity.this, R.string.app_name, R.string.splash_screen_activity_photos_error);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

        finish();
    }
}
