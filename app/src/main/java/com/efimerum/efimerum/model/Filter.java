package com.efimerum.efimerum.model;


public class Filter implements BubbleItem {
    private int id;
    private String text;

    public Filter() {
    }

    public int getId() {
        return id;
    }

    public Filter setId(int id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public Filter setText(String text) {
        this.text = text;
        return this;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public String getBubbleText() {
        return text;
    }
}
