package com.efimerum.efimerum.model.mappers;


import com.efimerum.efimerum.manager.db.DBTag;
import com.efimerum.efimerum.model.Tag;

public class TagDBTagMapper {
    public DBTag tagToDBTag(Tag tag) {
        DBTag dbTag = new DBTag();
        dbTag.setName(tag.getText());
        return dbTag;
    }

    public Tag dbTagToTag(DBTag dbTag) {
        Tag tag = new Tag(dbTag.getName());
        return tag;
    }
}
