package com.efimerum.efimerum.model.mappers;


import com.efimerum.efimerum.manager.db.DBPhoto;
import com.efimerum.efimerum.manager.db.DBTag;
import com.efimerum.efimerum.model.ImageData;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Tag;

import java.util.Date;

public class PhotoDBPhotoMapper {

    private TagDBTagMapper mapper;

    public PhotoDBPhotoMapper() {
        mapper = new TagDBTagMapper();
    }

    public DBPhoto photoToDBPhoto(Photo photo) {
        DBPhoto dbPhoto = new DBPhoto();

        dbPhoto.setId(photo.getUuid());
        dbPhoto.setCreation_date(photo.getCreated().getTime());
        dbPhoto.setExpiration_date(photo.getExpires().getTime());
        dbPhoto.setImage_height(photo.getImage().getHeight());
        dbPhoto.setImage_url(photo.getImage().getUrl());
        dbPhoto.setImage_height(photo.getImage().getHeight());
        dbPhoto.setLatitude(photo.getLatitude());
        dbPhoto.setLongitude(photo.getLongitude());
        dbPhoto.setOwner(photo.getOwner());
        dbPhoto.setLikes_count(photo.getLikes());
        dbPhoto.setThumb_height(photo.getThumbnail().getHeight());
        dbPhoto.setThumb_url(photo.getThumbnail().getUrl());
        dbPhoto.setThumb_width(photo.getThumbnail().getWidth());
        dbPhoto.setSort_field(photo.getMd5());
        dbPhoto.setShare_url(photo.getShareURL());

        for (Tag tag: photo.getTags()) {
            dbPhoto.getTags().add(mapper.tagToDBTag(tag));
        }

        return dbPhoto;
    }

    public Photo dbPhotoToPhoto(DBPhoto dbPhoto) {
        Photo photo = new Photo(dbPhoto.getId());

        photo.setCreated(new Date(dbPhoto.getCreation_date()));
        photo.setExpires(new Date(dbPhoto.getExpiration_date()));
        photo.setLatitude(dbPhoto.getLatitude());
        photo.setLongitude(dbPhoto.getLongitude());
        photo.setLikes(dbPhoto.getLikes_count());
        photo.setOwner(dbPhoto.getOwner());
        photo.setMd5(dbPhoto.getSort_field());
        photo.setShareURL(dbPhoto.getShare_url());

        ImageData imageData = new ImageData(dbPhoto.getImage_url(), dbPhoto.getImage_height(), dbPhoto.getImage_width());
        photo.setImage(imageData);

        ImageData thumbImageData = new ImageData(dbPhoto.getThumb_url(), dbPhoto.getThumb_height(), dbPhoto.getThumb_width());
        photo.setThumbnail(thumbImageData);

        for (DBTag dbTag: dbPhoto.getTags()) {
            photo.getTags().add(mapper.dbTagToTag(dbTag));
        }

        return photo;
    }
}
