package com.efimerum.efimerum.model;

public class User {
    private String name;
    private String email;
    private String uid;
    private String photoUrl;
    private String fcmToken;
    private boolean likeNotificationEnabled;

    public User(String uid) {
        this.uid = uid;
    }

    public User(String email, String name, String photoUrl) {
        this.email = email;
        this.name = name;
        this.photoUrl = photoUrl;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public User setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public User setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        return this;
    }

    public boolean isAnonymous() {
        return email == null || email.isEmpty();
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public User setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
        return this;
    }

    public boolean isLikeNotificationEnabled() {
        return likeNotificationEnabled;
    }

    public User setLikeNotificationEnabled(boolean likeNotificationEnabled) {
        this.likeNotificationEnabled = likeNotificationEnabled;
        return this;
    }
}
