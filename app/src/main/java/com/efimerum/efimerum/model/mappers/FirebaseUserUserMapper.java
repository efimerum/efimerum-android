package com.efimerum.efimerum.model.mappers;

import com.efimerum.efimerum.manager.firebase.FBUser;
import com.efimerum.efimerum.model.User;

public class FirebaseUserUserMapper {
    public User map(FBUser firebaseUser) {
        if (firebaseUser == null) {
            return null;
        }

        User user = new User(firebaseUser.uuid);
        user.setEmail(firebaseUser.email)
                .setName(firebaseUser.name)
                .setPhotoUrl(firebaseUser.profileImageURL)
                .setFcmToken(firebaseUser.fcmToken)
                .setLikeNotificationEnabled(firebaseUser.likeNotificationEnabled);

        return user;
    }

    public FBUser unMap(User user) {
        if (user == null) {
            return null;
        }

        FBUser firebaseUser = new FBUser(user.getUid());
        firebaseUser.email = user.getEmail();
        firebaseUser.name = user.getName();
        firebaseUser.profileImageURL = user.getPhotoUrl();
        firebaseUser.fcmToken = user.getFcmToken();
        firebaseUser.likeNotificationEnabled = user.isLikeNotificationEnabled();

        return firebaseUser;
    }
}
