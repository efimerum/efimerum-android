package com.efimerum.efimerum.model;

import java.io.Serializable;

public class Tag implements BubbleItem, Serializable {
    private String text;

    public Tag() {
    }

    public Tag(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public Tag setText(String text) {
        this.text = text;
        return this;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public String getBubbleText() {
        return text;
    }
}
