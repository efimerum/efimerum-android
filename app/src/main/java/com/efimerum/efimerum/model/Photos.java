package com.efimerum.efimerum.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Photos implements Serializable {
    private List<Photo> list;

    private Photos() {
    }

    public void setList(List<Photo> list) {
        this.list = list;
    }

    public long size() {
        return list.size();
    }

    public @Nullable
    Photo get(long index) {
        if (index > size() || index < 0) {
            return null;
        }
        return list.get((int)index);
    }

    public void set(long index, Photo photo) {
        if (index > size() || index < 0) {
            return;
        }

        list.set((int) index, photo);
    }

    public static @NonNull
    Photos build(@NonNull List<Photo> baseModelList) {
        Photos photos = new Photos();

        if (baseModelList == null) {
            photos.setList(new ArrayList<Photo>());
        } else {
            photos.setList(baseModelList);
        }

        return photos;
    }

    public static @NonNull
    Photos build(@NonNull Photo photo) {
        List<Photo> list = new ArrayList<>();
        list.add(photo);
        return build(list);
    }
}
