package com.efimerum.efimerum.model;


import java.io.Serializable;

public class ImageData implements Serializable{

    private String url;
    private int height;
    private int width;

    public ImageData(String url, int height, int width) {
        this.url = url;
        this.height = height;
        this.width = width;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public double getAspectRatio() {
        return (double)width / (double)height;
    }
}
