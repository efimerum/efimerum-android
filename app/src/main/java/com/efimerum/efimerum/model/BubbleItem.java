package com.efimerum.efimerum.model;


public interface BubbleItem {
    String getBubbleText();
}
