package com.efimerum.efimerum.model;

import com.efimerum.efimerum.util.CommonFunctions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Photo implements Serializable {
    private String uuid;
    private Date created;
    private Date expires;
    private double latitude;
    private double longitude;
    private long likes;
    private String owner;
    private ImageData image;
    private ImageData thumbnail;
    private String md5;
    private String shareURL;
    private boolean deleted = false;
    private List<Tag> tags = new ArrayList<>();

    public Photo(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public Photo setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public Date getCreated() {
        return created;
    }

    public Photo setCreated(Date created) {
        this.created = created;
        return this;
    }

    public Date getExpires() {
        return expires;
    }

    public Photo setExpires(Date expires) {
        this.expires = expires;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public Photo setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public Photo setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public long getLikes() {
        return likes;
    }

    public Photo setLikes(long likes) {
        this.likes = likes;
        return this;
    }

    public String getOwner() {
        return owner;
    }

    public Photo setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public ImageData getImage() {
        return image;
    }

    public Photo setImage(ImageData image) {
        this.image = image;
        return this;
    }

    public ImageData getThumbnail() {
        return thumbnail;
    }

    public Photo setThumbnail(ImageData thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public String getMd5() {
        return md5;
    }

    public Photo setMd5(String md5) {
        this.md5 = md5;
        return this;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public Photo setTags(List<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public String getShareURL() {
        return shareURL;
    }

    public void setShareURL(String shareURL) {
        this.shareURL = shareURL;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getLikesString() {
        return CommonFunctions.longToShortString(this.likes);
    }
}
