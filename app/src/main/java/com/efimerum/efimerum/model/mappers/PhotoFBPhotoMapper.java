package com.efimerum.efimerum.model.mappers;


import com.efimerum.efimerum.manager.firebase.FBPhoto;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Tag;
import com.efimerum.efimerum.util.Constants;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PhotoFBPhotoMapper {
    public Photo map(FBPhoto firebasePhoto) {
        if (firebasePhoto == null) {
            return null;
        }

        Photo photo = new Photo(firebasePhoto.uuid);
        if (firebasePhoto.creationDate > 0) {
            photo.setCreated(new Date(firebasePhoto.creationDate * 1000));
        }
        if (firebasePhoto.expirationDate > 0) {
            photo.setExpires(new Date(firebasePhoto.expirationDate * 1000));
        }

        photo.setShareURL(firebasePhoto.dynamicLink);
        photo.setLatitude(firebasePhoto.latitude);
        photo.setLongitude(firebasePhoto.longitude);
        photo.setLikes(firebasePhoto.numOfLikes);
        photo.setOwner(firebasePhoto.owner);
        photo.setMd5(firebasePhoto.md5);
        photo.setImage(new ImageDataFBImageData().map(firebasePhoto.imageData));
        photo.setThumbnail(new ImageDataFBImageData().map(firebasePhoto.thumbnailData));

        if (firebasePhoto.labels != null && firebasePhoto.labels.containsKey(Constants.appLanguage)) {
            List<String> tagsNames = new ArrayList<>(firebasePhoto.labels.get(Constants.appLanguage).keySet());
            for (String tagName : tagsNames) {
                photo.getTags().add(new Tag(tagName));
            }
        }

        return photo;
    }
}
