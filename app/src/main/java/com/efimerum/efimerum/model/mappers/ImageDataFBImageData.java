package com.efimerum.efimerum.model.mappers;


import com.efimerum.efimerum.manager.firebase.FBImageData;
import com.efimerum.efimerum.model.ImageData;

public class ImageDataFBImageData {
    public ImageData map(FBImageData firebaseImageData) {
        if (firebaseImageData == null) {
            return null;
        }

        return new ImageData(firebaseImageData.url, firebaseImageData.height, firebaseImageData.width);
    }
}
