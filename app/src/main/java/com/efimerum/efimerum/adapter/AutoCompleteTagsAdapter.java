package com.efimerum.efimerum.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;
import android.widget.Filter;

import com.efimerum.efimerum.model.Tag;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteTagsAdapter extends ArrayAdapter<Tag> {
    Filter filter = null;
    List<Tag> initialObjects;

    public AutoCompleteTagsAdapter(Context context, final int resource, List<Tag> objects) {
        super(context, resource, objects);

        if (objects != null) {
            initialObjects = new ArrayList<>(objects);
        }

        filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if(constraint != null && initialObjects != null) {
                    List<Tag> filteredTags = new ArrayList<>();

                    for (Tag tag: initialObjects) {
                        if (tag.getText().toUpperCase().indexOf(constraint.toString().toUpperCase()) != -1) {
                            filteredTags.add(tag);
                        }
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filteredTags;
                    filterResults.count = filteredTags.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if(results != null && results.count > 0) {
                    List<Tag> validTags = (List<Tag>) results.values;
                    clear();
                    addAll(validTags);
                    notifyDataSetChanged();
                }
            }
        };
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return filter;
    }
}
