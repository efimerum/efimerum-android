package com.efimerum.efimerum.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.efimerum.efimerum.fragment.PhotoGridFragment;
import com.efimerum.efimerum.model.Photos;

import java.util.ArrayList;

public class UserProfileFragmentPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[]{"My Photos", "My Likes"};
    private ArrayList<Fragment> lista;

    public UserProfileFragmentPagerAdapter(final FragmentManager fm, final Photos myPostedPhotos, final Photos myLikedPhotos) {
        super(fm);
        lista = new ArrayList<>();
        lista.add(PhotoGridFragment.newInstance(myPostedPhotos, PhotoGridFragment.SMALL_ROW_HEIGHT));
        lista.add(PhotoGridFragment.newInstance(myLikedPhotos, PhotoGridFragment.SMALL_ROW_HEIGHT));
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return lista.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}