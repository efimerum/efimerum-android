package com.efimerum.efimerum.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.fivehundredpx.greedolayout.GreedoLayoutSizeCalculator;
import com.fivehundredpx.greedolayout.Size;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import jp.wasabeef.picasso.transformations.BlurTransformation;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.PhotoViewHolder> implements GreedoLayoutSizeCalculator.SizeCalculatorDelegate {

    private GreedoLayoutSizeCalculator sizeCalculator;
    private WeakReference<Context> context;
    private Photos photos;
    private OnPhotoclickedListener listener;

    public PhotosAdapter(Context context, Photos photos) {
        this.context = new WeakReference<Context>(context);
        this.photos = photos;
    }

    public int updatePhoto(Photo updatedPhoto) {
        for (int i = 0; i < this.photos.size(); i++) {
            Photo photo = this.photos.get(i);
            if (photo.getUuid().equals(updatedPhoto.getUuid())) {
                this.photos.set(i, updatedPhoto);
                return i;
            }
        }

        return -1;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public void setOnPhotoclickedListener(OnPhotoclickedListener listener) {
        this.listener = listener;
    }

    public void setSizeCalculator(GreedoLayoutSizeCalculator sizeCalculator) {
        this.sizeCalculator = sizeCalculator;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(this.context.get()).inflate(R.layout.adapter_photos_item, parent, false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, final int position) {
        final Photo photo = this.photos.get(position);

        Size imageViewSize = sizeCalculator.sizeForChildAtPosition(position);
        holder.itemView.getLayoutParams().width = imageViewSize.getWidth();
        holder.itemView.getLayoutParams().height = imageViewSize.getHeight();

        holder.likesTextView.setText(photo.getLikesString());

        if (photo.isDeleted()) {
            holder.photoInfoContainerLayout.setVisibility(View.GONE);
            Picasso.with(context.get())
                    .load(photo.getThumbnail().getUrl())
                    .transform(new BlurTransformation(context.get(), 25, 1))
                    .into(holder.photoImageView);
        } else {
            holder.photoInfoContainerLayout.setVisibility(View.VISIBLE);
            Picasso.with(context.get())
                    .load(photo.getThumbnail().getUrl())
                    .into(holder.photoImageView);

            holder.photoImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClick(photo, position);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return (int) this.photos.size();
    }

    @Override
    public double aspectRatioForIndex(int index) {
        if (index >= getItemCount()) return 1.0;
        return this.photos.get(index).getThumbnail().getAspectRatio();
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        private ImageView photoImageView;
        private TextView likesTextView;
        private LinearLayout photoInfoContainerLayout;

        public PhotoViewHolder(View view) {
            super(view);
            this.photoImageView = (ImageView) view.findViewById(R.id.photos_adapter_item_image);
            this.likesTextView = (TextView) view.findViewById(R.id.photos_adapter_item_likes_text);
            this.photoInfoContainerLayout = (LinearLayout) view.findViewById(R.id.photos_adapter_item_info_container);
        }
    }

    public interface OnPhotoclickedListener {
        void onClick(Photo photo, int position);
    }
}
