package com.efimerum.efimerum.navigator;


import android.content.Intent;

import com.efimerum.efimerum.activity.LoginActivity;
import com.efimerum.efimerum.activity.MainActivity;
import com.efimerum.efimerum.activity.MapActivity;
import com.efimerum.efimerum.activity.PhotoActivity;
import com.efimerum.efimerum.activity.SettingsActivity;
import com.efimerum.efimerum.activity.SplashScreenActivity;
import com.efimerum.efimerum.activity.UserProfileActivity;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.util.Constants;

public class Navigator {
    public static Intent navigateFromSplashScreenActivityToMainActivity(final SplashScreenActivity splashScreenActivity) {
        final Intent i = new Intent(splashScreenActivity, MainActivity.class);
        splashScreenActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromMainActivityToPhotoActivity(final MainActivity mainActivity, Photos photos, int index) {
        final Intent i = new Intent(mainActivity, PhotoActivity.class);

        i.putExtra(Constants.INTENT_KEY_PHOTOS, photos);
        i.putExtra(Constants.INTENT_KEY_PHOTO_INDEX, index);

        mainActivity.startActivityForResult(i, Constants.SHOW_PHOTO_REQUEST_CODE);

        return i;
    }

    public static Intent navigateFromMainActivityToPhotoActivityWithPhotoKey(final MainActivity mainActivity, String photoKey) {
        final Intent i = new Intent(mainActivity, PhotoActivity.class);

        i.putExtra(Constants.INTENT_BACKEND_KEY_PHOTO_UUID, photoKey);

        mainActivity.startActivityForResult(i, Constants.SHOW_PHOTO_REQUEST_CODE);

        return i;
    }

    public static Intent navigateFromMainActivityToLoginActivity(final MainActivity mainActivity, int action) {
        final Intent i = new Intent(mainActivity, LoginActivity.class);
        i.putExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, action);

        mainActivity.startActivityForResult(i, Constants.LOGIN_REQUEST_CODE);

        return i;
    }

    public static Intent navigateFromMainActivityToUserProfileActivity(final MainActivity mainActivity) {
        final Intent i = new Intent(mainActivity, UserProfileActivity.class);

        mainActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromUserProfileActivityToSettingsActivity(final UserProfileActivity userProfileActivity) {
        final Intent i = new Intent(userProfileActivity, SettingsActivity.class);

        userProfileActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromUserProfileActivityToPhotoActivity(final UserProfileActivity userProfileActivity, Photos photos, int index) {
        final Intent i = new Intent(userProfileActivity, PhotoActivity.class);

        i.putExtra(Constants.INTENT_KEY_PHOTOS, photos);
        i.putExtra(Constants.INTENT_KEY_PHOTO_INDEX, index);
        i.putExtra(Constants.INTENT_SHOW_LIKE_DISLIKE, "false");

        userProfileActivity.startActivityForResult(i, Constants.SHOW_PHOTO_REQUEST_CODE);

        return i;
    }

    public static Intent navigateFromSettingsActivityToMainActivity(final SettingsActivity settingsActivity) {
        final Intent i = new Intent(settingsActivity, MainActivity.class);

        settingsActivity.startActivity(i);

        return i;
    }

    public static Intent navigateFromPhotoActivityToLoginActivity(final PhotoActivity photoActivity, Photo photo, int action) {
        final Intent i = new Intent(photoActivity, LoginActivity.class);
        i.putExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_ACTION, action);
        if (photo != null) {
            i.putExtra(Constants.INTENT_KEY_LOGIN_RESPONSE_DATA, photo);
        }

        photoActivity.startActivityForResult(i, Constants.LOGIN_REQUEST_CODE);

        return i;
    }

    public static Intent navigateFromPhotoActivityToMapActivity(final PhotoActivity photoActivity, Photo photo) {
        final Intent i = new Intent(photoActivity, MapActivity.class);
        if (photo != null) {
            i.putExtra(Constants.INTENT_KEY_PHOTO, photo);
        }

        photoActivity.startActivity(i);

        return i;
    }
}
