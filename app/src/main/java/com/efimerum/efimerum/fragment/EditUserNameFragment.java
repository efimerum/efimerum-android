package com.efimerum.efimerum.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;

public class EditUserNameFragment extends DialogFragment {

    public static final int RESULT_OK = 0;
    public static final int RESULT_CANCEL = 1;

    protected EditText tv;

    public static EditUserNameFragment newInstance() {
        Bundle args = new Bundle();

        EditUserNameFragment fragment = new EditUserNameFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog_Alert);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setTitle("Edit user name");

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_edit_user_name, container, false);

        tv = (EditText) root.findViewById(R.id.edit_user_name_fragment_et);
        tv.setText(((EfimerumApp) getActivity().getApplication()).getCurrentUser().getName());

        Button okBtn = (Button) root.findViewById(R.id.edit_user_name_fragment_ok_button);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(RESULT_OK);
            }
        });

        Button cancelBtn = (Button) root.findViewById(R.id.edit_user_name_fragment_cancel_button);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(RESULT_CANCEL);
            }
        });

        return root;
    }

    private void returnDialogResult(int code) {
        if (getActivity() instanceof EditUserNameDialogListener) {
            EditUserNameDialogListener listener = (EditUserNameDialogListener) getActivity();
            listener.editResult(tv.getText().toString(), code);
        }
        dismiss();
    }

    public interface EditUserNameDialogListener {
        void editResult(String newName, int resultCode);
    }
}