package com.efimerum.efimerum.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.model.BanReason;
import com.efimerum.efimerum.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class ReportFragment extends DialogFragment {

    private static final String PHOTO_KEY = "PHOTO_KEY";
    private List<BanReason> reasons;
    private Photo photo;

    public static ReportFragment newInstance(Photo photo) {
        Bundle args = new Bundle();
        args.putSerializable(PHOTO_KEY, photo);

        ReportFragment fragment = new ReportFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog_Alert);

        if (getArguments() != null) {
            photo = (Photo) getArguments().getSerializable(PHOTO_KEY);
        }

        reasons = new ArrayList<>();
        for (String code : getResources().getStringArray(R.array.report_codes)) {
            String codeName = getString(getResources().getIdentifier("report_code_" + code, "string", getActivity().getPackageName()));
            BanReason reason = new BanReason();
            reason.setCode(code);
            reason.setName(codeName);
            reasons.add(reason);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setTitle(R.string.report_fragment_dialog_title);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_report, container, false);
        final Spinner spinner = (Spinner) root.findViewById(R.id.report_fragment_reason_selector);
        final ArrayAdapter<BanReason> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, reasons);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Button cancelBtn = (Button) root.findViewById(R.id.reason_fragment_cancel_button);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(null);
            }
        });

        Button aceptBtn = (Button) root.findViewById(R.id.reason_fragment_accept_button);
        aceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BanReason reason = (BanReason) spinner.getSelectedItem();
                returnDialogResult(reason);
            }
        });

        return root;
    }

    private void returnDialogResult(BanReason reason) {
        if (getActivity() instanceof PhotoBanDialogListener) {
            PhotoBanDialogListener listener = (PhotoBanDialogListener) getActivity();
            listener.BanResult(photo, reason);
        }
        dismiss();
    }

    public interface PhotoBanDialogListener {
        void BanResult(Photo photo, BanReason reason);
    }
}
