package com.efimerum.efimerum.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.efimerum.efimerum.EfimerumApp;
import com.efimerum.efimerum.R;
import com.efimerum.efimerum.adapter.PhotosAdapter;
import com.efimerum.efimerum.manager.communication.ServerEventsManager;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.model.Photos;
import com.efimerum.efimerum.util.CommonFunctions;
import com.fivehundredpx.greedolayout.GreedoLayoutManager;
import com.fivehundredpx.greedolayout.GreedoSpacingItemDecoration;

public class PhotoGridFragment extends Fragment implements ServerEventsManager.PhotosEventsListener {

    private static final String PHOTOS_ARG_KEY = "PHOTOS_ARGUMENT";
    private static final String ROW_HEIGHT_KEY = "ROW_HEIGHT_ARGUMENT";

    private static final int PHOTOS_SPAICING = 0;

    public static final int SMALL_ROW_HEIGHT = 150;
    public static final int BIG_ROW_HEIGHT = 250;

    private PhotosAdapter photosAdapter;
    private Photos photos;
    private int rowHeight;
    private PhotoGridFragmentEventsListener listener;
    private RecyclerView recyclerPhotos;

    public static PhotoGridFragment newInstance(Photos photos) {
        return newInstance(photos, BIG_ROW_HEIGHT);
    }

    public static PhotoGridFragment newInstance(Photos photos, int rowHeight) {
        PhotoGridFragment fragment = new PhotoGridFragment();

        Bundle arguments = new Bundle();
        arguments.putSerializable(PHOTOS_ARG_KEY, photos);
        arguments.putInt(ROW_HEIGHT_KEY, rowHeight);

        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            this.photos = (Photos) getArguments().getSerializable(PHOTOS_ARG_KEY);
            this.rowHeight = getArguments().getInt(ROW_HEIGHT_KEY, BIG_ROW_HEIGHT);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_photos_grid, container, false);
        recyclerPhotos = (RecyclerView) view.findViewById(R.id.photos_grid_fragment_recyclerview);

        initializeRecyclerView();

        EfimerumApp app = (EfimerumApp) getActivity().getApplication();
        app.getEventsManager().addPhotosListener(this);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        EfimerumApp app = (EfimerumApp) getActivity().getApplication();
        app.getEventsManager().removePhotosListener(this);
    }

    @Override
    public void photoAdded(Photo photo) {}

    @Override
    public void photoRemoved(Photo photo) {
        photo.setDeleted(true);
        updateAdapterPhoto(photo);
    }

    @Override
    public void photoModified(Photo photo) {
        updateAdapterPhoto(photo);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (getActivity() instanceof PhotoGridFragmentEventsListener) {
            listener = (PhotoGridFragmentEventsListener) getActivity();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        listener = null;
    }

    private void initializeRecyclerView() {
        photosAdapter = new PhotosAdapter(getActivity(), this.photos);
        photosAdapter.setOnPhotoclickedListener(new PhotosAdapter.OnPhotoclickedListener() {
            @Override
            public void onClick(Photo photo, int position) {
                if (listener != null) {
                    listener.onPhotoClicked(photo, position);
                }
            }
        });

        final GreedoLayoutManager layoutManager = new GreedoLayoutManager(photosAdapter);
        layoutManager.setMaxRowHeight(CommonFunctions.dpToPx(this.rowHeight, getActivity()));
        photosAdapter.setSizeCalculator(layoutManager.getSizeCalculator());

        recyclerPhotos.setLayoutManager(layoutManager);
        recyclerPhotos.setAdapter(photosAdapter);

        int spacing = CommonFunctions.dpToPx(PHOTOS_SPAICING, getActivity());
        recyclerPhotos.addItemDecoration(new GreedoSpacingItemDecoration(spacing));

        recyclerPhotos.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (listener != null) {
                    listener.onGridScrolling();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (listener != null) {
                    listener.onScrolled();
                }
            }
        });
    }

    private void updateAdapterPhoto(Photo photo) {
        int index = photosAdapter.updatePhoto(photo);
        if (index != -1) {
            photosAdapter.notifyItemChanged(index);
        }
    }

    public void scrollToTop() {
        recyclerPhotos.scrollToPosition(0);
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
        photosAdapter.setPhotos(photos);
        photosAdapter.notifyDataSetChanged();
    }

    public interface PhotoGridFragmentEventsListener {
        void onGridScrolling();
        void onScrolled();
        void onPhotoClicked(Photo photo, int position);
    }
}
