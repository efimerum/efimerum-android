package com.efimerum.efimerum.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.efimerum.efimerum.R;

public class EditAvatarFragment extends DialogFragment {

    public static final int RESULT_CAMERA = 0;
    public static final int RESULT_GALLERY = 1;
    public static final int RESULT_CANCEL = 2;

    public static EditAvatarFragment newInstance() {
        Bundle args = new Bundle();

        EditAvatarFragment fragment = new EditAvatarFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog_Alert);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.setTitle(R.string.edit_avatar_fragment_title_text);

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.fragment_edit_avatar, container, false);

        LinearLayout cameraBtn = (LinearLayout) root.findViewById(R.id.edit_avatar_fragment_camera_button);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(RESULT_CAMERA);
            }
        });

        LinearLayout galleryBtn = (LinearLayout) root.findViewById(R.id.edit_avatar_fragment_gallery_button);
        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(RESULT_GALLERY);
            }
        });

        Button cancelBtn = (Button) root.findViewById(R.id.edit_avatar_fragment_cancel_button);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnDialogResult(RESULT_CANCEL);
            }
        });

        return root;
    }

    private void returnDialogResult(int code) {
        if (getActivity() instanceof EditAvatarDialogListener) {
            EditAvatarDialogListener listener = (EditAvatarDialogListener) getActivity();
            listener.editResult(code);
        }
        dismiss();
    }

    public interface EditAvatarDialogListener {
        void editResult(int resultCode);
    }

}