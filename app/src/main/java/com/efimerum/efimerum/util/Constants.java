package com.efimerum.efimerum.util;


public class Constants {
    public static String appName = "com.efimerum.efimerum";
    public static String appLanguage = "EN";

    public static final String INTENT_KEY_LOGIN_RESPONSE_ACTION = appName + "INTENT_KEY_LOGIN_RESPONSE_ACTION";
    public static final String INTENT_KEY_LOGIN_RESPONSE_DATA = appName + "INTENT_KEY_LOGIN_RESPONSE_DATA";
    public static final String INTENT_KEY_PHOTOS = appName + "INTENT_KEY_PHOTOS";
    public static final String INTENT_KEY_PHOTO_INDEX = appName + "INTENT_KEY_PHOTO_INDEX";
    public static final String INTENT_KEY_PHOTO = appName + "INTENT_KEY_PHOTO";
    public static final String INTENT_BACKEND_KEY_PHOTO_UUID = "photoId";
    public static final String INTENT_SHOW_LIKE_DISLIKE = appName + "INTENT_SHOW_LIKE_DISLIKE";

    public static final int LOGIN_REQUEST_CODE = 100;
    public static final int SHOW_PHOTO_REQUEST_CODE = 101;
}
