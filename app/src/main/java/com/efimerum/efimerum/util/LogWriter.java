package com.efimerum.efimerum.util;


import android.util.Log;

public class LogWriter {
    public final static String TAG = "EFIMERUM";

    public static void writeMessage(String message) {
        Log.d(TAG, message);
    }

    public static void writeError(String message) {
        writeError(message, null);
    }

    public static void writeError(String message, Throwable error) {
        if (error == null) {
            Log.e(TAG, message);
        } else {
            String errorMessage = message;

            if (error.getMessage() != null) {
                errorMessage = errorMessage + ": " + error.getMessage();
            }

            Log.e(TAG, errorMessage, error);
        }
    }
}
