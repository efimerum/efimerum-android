package com.efimerum.efimerum.util;


import com.efimerum.efimerum.model.Photo;

public interface PhotoEventsListener {
    void photoViewed(int position, Photo photo, boolean liked);
    void photoClicked(Photo photo);
    void photoBanned(Photo photo);
    void photoShared(Photo photo);
}
