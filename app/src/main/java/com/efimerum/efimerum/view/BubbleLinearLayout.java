package com.efimerum.efimerum.view;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.model.BubbleItem;

public class BubbleLinearLayout extends LinearLayout {

    private BubbleItem item;

    public BubbleLinearLayout(Context context, BubbleItem item) {
        super(context);

        this.item = item;

        inflate(context, R.layout.layout_bubble_tag, this);
        TextView tagNameTextView = (TextView) findViewById(R.id.bubble_tag_textView);
        tagNameTextView.setText(this.item.toString());
    }

    public BubbleItem getItem() {
        return item;
    }
}
