package com.efimerum.efimerum.view;

import android.content.Context;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.efimerum.efimerum.R;
import com.efimerum.efimerum.model.Photo;
import com.efimerum.efimerum.util.PhotoEventsListener;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

@Layout(R.layout.view_photo)
public class PhotoView {

    @View(R.id.photo_view_photo_image)
    private ImageView photoImageView;
    @View(R.id.photo_view_dislike_image)
    private ImageView dislikeImageView;
    @View(R.id.photo_view_info_image)
    private ImageView infoImageView;
    @View(R.id.photo_view_like_image)
    private ImageView likeImageView;
    @View(R.id.photo_view_share_image)
    private ImageView shareImageView;
    @View(R.id.photo_view_ban_image)
    private ImageView banImageView;
    @View(R.id.photo_view_button_bar_layout)
    private FrameLayout buttonsLayout;

    private WeakReference<Context> context;
    private Photo photo;
    private SwipePlaceHolderView swipeView;
    private PhotoEventsListener listener;
    private int position;

    private boolean showLikeDislike;

    public PhotoView(Context context, final Photo photo, SwipePlaceHolderView swipeView, int position, boolean showLikeDislike) {
        this.context = new WeakReference<Context>(context);
        this.photo = photo;
        this.position = position;
        this.swipeView = swipeView;
        this.showLikeDislike = showLikeDislike;
    }

    public void setListener(PhotoEventsListener listener) {
        this.listener = listener;
    }

    @Resolve
    private void onResolved() {
        Picasso.with(context.get()).load(photo.getImage().getUrl()).into(photoImageView);

        if (!showLikeDislike) {
            likeImageView.setVisibility(android.view.View.INVISIBLE);
            dislikeImageView.setVisibility(android.view.View.INVISIBLE);
        }

        initializeClickListeners();
    }

    @SwipeOut
    private void onSwipedOut() {
        if (listener != null) {
            listener.photoViewed(position, photo, false);
        }
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        this.buttonsLayout.setVisibility(android.view.View.VISIBLE);
    }

    @SwipeIn
    private void onSwipeIn() {
        if (listener != null) {
            listener.photoViewed(position, photo, true);
        }
    }

    @SwipeInState
    private void onSwipeInState() {
        this.buttonsLayout.setVisibility(android.view.View.GONE);
    }

    @SwipeOutState
    private void onSwipeOutState() {
        this.buttonsLayout.setVisibility(android.view.View.GONE);
    }

    private void initializeClickListeners() {

        this.buttonsLayout.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                // filter touches when underlying view is Obscured by this view.
            }
        });

        this.photoImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                if (listener != null) {
                    listener.photoClicked(photo);
                }
            }
        });

        this.infoImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                if (listener != null) {
                    listener.photoClicked(photo);
                }
            }
        });

        this.shareImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                if (listener != null) {
                    listener.photoShared(photo);
                }
            }
        });

        this.banImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                if (listener != null) {
                    listener.photoBanned(photo);
                }
            }
        });

        this.likeImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                swipeView.doSwipe(true);
            }
        });

        this.dislikeImageView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                swipeView.doSwipe(false);
            }
        });
    }
}
